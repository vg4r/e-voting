var questionnaire_variant_container = $("#questionnaire-variant-container");

var container1 = questionnaire_variant_container.find('#container-1');

var container2 = questionnaire_variant_container.find('#container-2');

function appendVotes1(list) {
    var html = '';
    $.each(list, function (index, value){
        html += '<div questionnaire-variat="'+ value.id +'" class="col-md-12 m-b-10"><button action-button onclick="questionnaireVariantOnclick(this)" vote-value="6" class="btn btn-lg btn-success m-r-10 vote-button text-white w-100">'+ value.content +'</button></div>'
    });

    $("#questionnaire-variant-container").find('#container-1-inner').html(html).show();
    container1.show();
    container2.hide();
}

function appendVotesSplitted(list) {
    var html = '';
    $.each(list, function (index, value){
        html += '<div class="row m-b-10 m-t-10">' +
            '<div class="col-md-3 text-center p-10">' +
            value.content +
            '</div>' +
            '<div questionnaire-variat="'+ value.id +'" class="col-md-9 text-center">' +
            '<button action-button onclick="questionnaireVariantOnclick(this)" vote-value="1" class="btn btn-lg btn-success m-r-10 vote-button text-white">1</button>' +
            '<button action-button onclick="questionnaireVariantOnclick(this)" vote-value="2" class="btn btn-lg btn-success m-r-10 vote-button text-white">2</button>' +
            '<button action-button onclick="questionnaireVariantOnclick(this)" vote-value="3" class="btn btn-lg btn-success m-r-10 vote-button text-white">3</button>' +
            '<button action-button onclick="questionnaireVariantOnclick(this)" vote-value="4" class="btn btn-lg btn-success m-r-10 vote-button text-white">4</button>' +
            '<button action-button onclick="questionnaireVariantOnclick(this)" vote-value="5" class="btn btn-lg btn-success m-r-10 vote-button text-white">5</button>' +
            '<button action-button onclick="questionnaireVariantOnclick(this)" vote-value="6" class="btn btn-lg btn-success m-r-10 vote-button text-white">6</button>' +
            '</div>' +
            '</div>';
    });
    $("#questionnaire-variant-container").find('#container-2').html(html);
    container2.show();
    container1.hide();
}

function appendVotes(list, style) {
    var styles = {
        0 : function () {
            appendVotesSplitted(list)
        },
        1 : function () {
            appendVotes1(list);
        }
    };
    styles[style]();
}

function appendQuestionnaire(quest) {
    var questionnaire_content = $("#questionnaire-content");
    questionnaire_content.html(quest.content);
    questionnaire_content.data("questionnaire", quest);

    var qn = $("#questionnaire-vote-note");
    var sp = $("#split-vote");
    container1.hide();
    container2.hide();
    var  style = 0; // splited
    if (quest.typeId == 100){
        style = 1; // not splited
        sp.show();
        sp.html(ConstantWords.SplitVote);
        var con = $("#questionnaire-variant-container");
        con.attr('data-type-style', style);
        $("#votes-2-allocate").html("");
        //splitVoteOnClick(sp, style);
        appendVotes(quest.questionnaireVariants, style);
        $("#abstain-button-cont").hide();
        $("#confirm-button-text").html(ConstantWords.Confirm);
    }else if (quest.typeId == 101){
        style = 0; // splited
        sp.hide();
        appendVotes(quest.questionnaireVariants, style);
        $("#abstain-button-cont").show();
        $("#confirm-button-text").html(ConstantWords.Confirm0);
        $("#votes-2-allocate").html(ConstantWords.Votes2Alloc + quest.voteCount);
    }else if (quest.typeId == 102){
        style = 0; // splited
        sp.hide();
        appendVotes(quest.questionnaireVariants, style);
        $("#abstain-button-cont").show();
        $("#confirm-button-text").html(ConstantWords.Confirm0);
        $("#votes-2-allocate").html(ConstantWords.Votes2Alloc + quest.voteCount);
        $("#questionnaire-variant-container").attr("data-type-style", "1");
    }
    $("#questionnaire-variant-container").attr("data-type-style", style);

    qn.html(questionnaireTypeProperties[quest.typeId].note[style].replace("#", quest.voteCount));
    $("#confirm-button").attr("data-answer-action", 'confirm');

}

function confirmOnclick() {
    var confirm = $("#confirm-button");
    var abstain = $("#abstain-button");
    var action = confirm.attr("data-answer-action");
    var actions = {
        confirm : function () {
            var quest = $("#questionnaire-content").data("questionnaire");
            var questionnaire_movement_id = quest.questionnaireMovementId;
            var questionnaire_answers = [];
            var currentContainer = questionnaire_variant_container.find('[container]:visible');
            var voteSum = 0;
            $.each(currentContainer.find('[questionnaire-variat]'), function (i, val) {
                var vv = $(val);
                if (vv.find("[vote-value][selected]").length > 0){
                    var voteCount2 = parseInt(vv.find("[vote-value][selected]").attr("vote-value"));
                    questionnaire_answers.push({
                        variantId : parseInt(vv.attr("questionnaire-variat")),
                        voteCount : voteCount2
                    });
                    voteSum += voteCount2;
                }
            });

            if (questionnaireTypeProperties[quest.typeId].ok(voteSum, quest.voteCount )){
                $("#questionnaire-vote-note").html(ConstantWords.PleaseWaitVE);

                confirm.attr('data-answer-action', 'disabled');
                $("[action-button]").addClass('pointer-event-none');
                confirm.removeClass('pointer-event-none');

                $.socket('send', { action : 'ANSWER', body : {
                    questionnaire_movement_id : questionnaire_movement_id,
                    questionnaire_answers : {
                        questionnaireAnswerList : questionnaire_answers
                    }
                }});
            } else {

                var noti = $.notify1(questionnaireTypeProperties[quest.typeId].error, messagesTypes.ERROR, messagesStyles.style2);

            }
        },
        disabled : function () {
            $.notify1(ConstantWords.PleaseWaitVE,messagesTypes.ERROR, messagesStyles.style2);
        },
        back2Voting : function () {
            var currentContainer = questionnaire_variant_container.find('[container]:visible');
            var quest = $("#questionnaire-content").data("questionnaire");
            currentContainer.find('button[vote-value]').each(function () {
                var t = $(this);
                t.removeAttr('disabled');
                t.removeAttr('selected');
            });

            $("[action-button]").removeClass('pointer-event-none');

            confirm.attr('data-answer-action', 'confirm');
            $("#confirm-button-text").html(ConstantWords.Confirm0);
            $("#votes-2-allocate").html(ConstantWords.Votes2Alloc + quest.voteCount);

            $(document).trigger('remove-all-notifications');
        }
    };
    actions[action]()
};

function abstainOnclick() {
    var quest = $("#questionnaire-content").data("questionnaire");
    var questionnaire_movement_id = quest.questionnaireMovementId;
    var questionnaire_answers = [];
    var currentContainer = questionnaire_variant_container.find('[container]:visible');
    //var voteSum = 0;
    $.each(currentContainer.find('[questionnaire-variat]'), function (i, val) {
        var vv = $(val);
        if (vv.find("[vote-value][selected]").length > 0){
            //var voteCount2 = parseInt(vv.find("[vote-value][selected]").attr("vote-value"));
            questionnaire_answers.push({
                variantId : parseInt(vv.attr("questionnaire-variat")),
                voteCount : 0
            });
            //voteSum += voteCount2;
        }
    });

    $.socket('send', { action : 'ANSWER', body : {
        questionnaire_movement_id : questionnaire_movement_id,
        questionnaire_answers : {
            questionnaireAnswerList : questionnaire_answers
        }
    }});
}

function questionnaireVariantOnclick(th) {
    var b = $(th);
    var questionnaire = $("#questionnaire-content").data("questionnaire");

    var currentContainer = questionnaire_variant_container.find('[container]:visible');

    var quest = $("#questionnaire-content").data("questionnaire");

    var attr = b.attr('selected');


    var totalVote = 0;

    if (attr == undefined){
        var p = b.parent('[questionnaire-variat]');
        p.find('[vote-value]').removeAttr('selected');
        b.attr('selected', 'selected');

        var array = currentContainer.find('button[vote-value][selected]');

        $.each(array, function (i, v) {
            totalVote += parseInt($(v).attr("vote-value"));
        });

        $.each(currentContainer.find('button[vote-value]'), function (i, v) {
            var rr = parseInt($(v).attr("vote-value"));
            if( (rr + totalVote > quest.voteCount) && ($(v).attr("selected") == undefined) ){
                $(v).attr('disabled', 'disabled');
                console.log("disabled : total = " + totalVote + ", vote = " + rr);
            } else {
                $(v).removeAttr('disabled');
            }
        });
    } else {
        b.removeAttr('selected');

        var array = currentContainer.find('button[vote-value][selected]');


        $.each(array, function (i, v) {
            totalVote += parseInt($(v).attr("vote-value"));
        });

        $.each(currentContainer.find('button[vote-value]'), function (i, v) {
            var rr = parseInt($(v).attr("vote-value"));

            //alert(rr + totalVote + ', '+ quest.voteCount + ', '+ $(v).attr("selected"));
            if( (rr + totalVote <= quest.voteCount) && ($(v).attr("selected") == undefined) ){
                $(v).removeAttr('disabled');
            }
        });
    }

    var mode = $("#questionnaire-variant-container").attr("data-type-style");
    if (mode == "0"){
        $("#votes-2-allocate").html(ConstantWords.Votes2Alloc + (quest.voteCount - totalVote));
        $("#confirm-button-text").html( ConstantWords.ConfirmXvotes.replace("#", totalVote));
    } else {
        $("#confirm-button-text").html(ConstantWords.Confirm + " " + b.html());
    }
}

function splitVoteOnClick(th) {
    var el = $(th);
    var con = $("#questionnaire-variant-container");
    var quest = $("#questionnaire-content").data("questionnaire");
    var style = con.attr('data-type-style');
    if (style == '1'){ // not splited

        el.html(ConstantWords.Back2NormalVote);
        con.attr('data-type-style', '0');

        appendVotesSplitted(quest.questionnaireVariants);
        $("#votes-2-allocate").html(ConstantWords.Votes2Alloc + quest.voteCount);
        $("#confirm-button-text").html(ConstantWords.Confirm0);

    } else if (style == '0'){

        el.html(ConstantWords.SplitVote);
        con.attr('data-type-style', '1');

        $("#votes-2-allocate").html("");
        $("#confirm-button-text").html(ConstantWords.Confirm);
        appendVotes1(quest.questionnaireVariants);
    }
    $("#questionnaire-vote-note").html(questionnaireTypeProperties[quest.typeId].note[style].replace("#", quest.voteCount));
}