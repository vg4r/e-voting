var pageCont = $(Constants.MODULE_PAGE_CONTAINER);
//var data_table_survey_members;
var operations = {

    BUTTTON_TYPE_OUTER : 'outer',
    BUTTTON_TYPE_INNER : 'inner',

    'new' : {
        'title' : 'New',
        'type'  : 'outer' ,
        'icon' : '<i class="material-icons f-30 text-purple-300">add</i>'
    },
    'edit' : {
        'title' : 'Edit',
        'type': 'inner',
        'icon' : '<i class="material-icons f-25 text-purple-300">edit</i>'
    },
    'delete' : {
        'title' : 'Delete',
        'type': 'inner',
        'icon' : '<i class="material-icons f-25 text-purple-300">delete</i>'
    },
    'reset-password' : {
        'title' : 'Reset password',
        'type' : 'inner',
        'class' : 'zmdi zmdi-eye',
        'icon' : '<i class="material-icons f-25 text-purple-300">lock_open</i>'
    },
    'view' : {
        'title' : 'View',
        'type' : 'inner',
        'class' : 'zmdi zmdi-eye',
        'icon' : '<i class="material-icons f-25 text-purple-300">visibility</i>'
    },
    'send' : {
        'title' : 'Send',
        'type' : 'inner',
        'class' : 'zmdi zmdi-mail-send',
        'icon' : '<i class="material-icons f-25 text-purple-300">send</i>'
    },
    'stop' : {
        'title' : 'Stop',
        'type' : 'inner',
        'class' : 'zmdi zmdi-block',
        'icon' : '<i class="material-icons f-25 text-red-600">stop</i>'
    },
    'interactive-report' : {
        'title' : 'Interactive report',
        'type' : 'outer',
        'class' : '',
        'icon' : '<i class="material-icons f-30 text-purple-300">track_changes</i>'
    },
    'print' : {
        'title' : 'Print',
        'type' : 'inner',
        'class' : '',
        'icon' : '<i class="material-icons f-30 text-purple-300">print</i>'
    },
    'print-all' : {
        'title' : 'Print',
        'type' : 'outer',
        'class' : '',
        'icon' : '<i class="material-icons f-30 text-purple-300">print</i>'
    }
};

var modules = {

    current_module : '',
    setCurrent : function (module){
        modules.current_module = module;
    },
    getCurrent : function (){
        return modules[modules.current_module];
    },

    'surveys': {
        current : false,
        filter : 'closed',
        name : 'Surveys',
        title : 'Surveys',
        link: 'pages/surveys/index.html',
        data_table : '',
        click: function (callback){
            pageCont.load(this.link, function () {
                modules['surveys'].data_table = $('#data-table-survey').DataTable({
                    "ajax" : {
                        url: "cs?action=common&sub_action=getDataContent&type=getSurveys"
                    },
                    "columnDefs": [
                        { targets: '_all', searchable: true, orderable : false}
                    ],
                    "columns": [
                        { "name": "#", "render" : retDataTableRowIndex},
                        { "name": "id", "data": "id", "visible" : false },
                        { "name": "name", "data": "name" },
                        { "name": "create_date", "data": "create_date" },
                        { "name": "end_date", "data": "end_date" },
                        /*{ "name": "update_user", "data": "update_user" , "visible" : false },
                        { "name": "update_user_id", "data": "update_user_id", "visible" : false },*/
                        { "name": "user_count",  "data": "user_count" },
                        { "name": "quest_count", "data": "quest_count" },
                        { "name": "activation", "data": "activation",  "render" : function (data, type, full, meta) {
                            console.log(full);
                            var sett = {
                                '1' : {
                                    class : 'text-green-600',
                                    text : 'Running'
                                },
                                '0' : {
                                    class : 'text-red-600',
                                    text : 'Completed'
                                }
                            };
                            var o = sett[full.activation];
                            return '<span class="'+  o.class +'"> '+ o.text +'</span>';
                        }},
                        {"name":"operations", "render":retDataTableRowOperations}
                    ]
                });
                if (callback) callback();
            });
        },
        loadFilter: function (callback){
            $(Constants.MODULE_FILTER_CONTAINER).load('pages/surveys/filter.html', function () {
                if (callback) callback();
            });
        },
        operations  : {
            'new' : {
                click : function () {
                    try {
                        var modal = $('#modal-dialog');
                        modal.load('pages/surveys/survey-oper-modal.html', function () {
                            surveyModalNew(modal);
                        })
                    }
                    catch(err) {
                        console.error(err);
                    }
                }
            },
            'edit' : {
                click : function (a) {
                    try {
                        var aa = $(a);
                        var tr = aa.parents('tr[role="row"]');
                        var surv = modules.getCurrent().data_table.row(tr).data();
                        var modal = $('#modal-dialog');

                        modal.load('pages/surveys/survey-oper-modal.html'+ '?current_timestamp_for_cache=' + new Date().getTime(), function () {

                            surveyModalEdit(modal, surv);

                            /*modal.userDefinedModal('open', {
                                events:{
                                    'on-open' : function () {
                                        $("#survey-name").val(surv.name);
                                        $('#data-table-survey-members').hide();
                                        $('#select_div').hide();
                                        $('#click_button').css("display", "none");


                                    }
                                },
                                buttons: {
                                    '#survey_operation' : function () {

                                    }
                                }
                            });*/
                        })
                    }
                    catch(err) {
                        console.error(err);
                    }
                }
            },
            'delete' : {
                click : function (a) {
                    var aa = $(a);
                    var tr = aa.parents('tr[role="row"]');
                    var surv = modules.getCurrent().data_table.row(tr).data();
                    $.confirm({action: function () {
                        $.ajax({
                            type : 'POST',
                            url : 'cs?action=survey&sub_action=deleteSurvey',
                            dataType : 'json',
                            data : {survey_id : surv.id},
                            success : function(data){
                                $.notify2(data);
                                modules.getCurrent().data_table.ajax.reload();
                            }
                        });
                    }});
                }
            },
            'stop' : {
                click : function (a) {
                    var aa = $(a);
                    var tr = aa.parents('tr[role="row"]');
                    var survey_id = modules.getCurrent().data_table.row(tr).data().id;

                    $.confirm({
                        title : "Are you sure to stop survey?",
                        text : "This will close all connected members and You will not be able to re enable this survey",
                        confirmButtonText : 'Yes stop this survey',
                        action: function () {
                        $.ajax({
                            type : 'POST',
                            url : 'cs?action=survey&sub_action=stopSurvey',
                            dataType : 'json',
                            data : {survey_id : survey_id},
                            success : function (data) {
                                $.notify2(data);
                                modules.getCurrent().data_table.ajax.reload();


                                $.socket('send', { action : 'CLOSE_SURVEY', body : {}});
                            }
                        })
                    }});
                }
            }
        }
    },

    'countries': {
        current : false,
        filter : 'closed',
        name : 'Countries',
        title : 'Countries',
        link: 'pages/countries/index.html',
        click: function (callback){
            pageCont.load(this.link, function () {
                modules['countries'].data_table = $('#data-table-countries').DataTable({
                    "columnDefs": [
                        { targets: '_all', searchable: true, orderable : false}
                    ],
                    "ajax": {
                        url : "cs?action=common&sub_action=getDataContent&type=getCountries"
                    },
                    "columns":[
                        {"name":"#", "render":retDataTableRowIndex},
                        {"name":"id", "data":"name", "visible": false},
                        {"name":"icon", "data":"icon" , render : function (data, type, full, meta) {
                            return "<img src='"+ data +"'/>"
                        }},
                        {"name":"name", "data":"name"},
                        /*{"name":"user_count", "data":"user_count"},*/
                        {"name":"operations", "render":retDataTableRowOperations}
                    ]
                });

                if (callback) callback();
            });
        },
        loadFilter: function (callback){
            $(Constants.MODULE_FILTER_CONTAINER).load('pages/countries/filter.html', function () {
                if (callback) callback();
            });
        },
        operations  : {
            'new' : {
                click : function (target) {
                    console.log(target);
                }
            },
            'edit' : {
                click : function (target) {
                    console.log(target);
                }
            },
            'delete' : {
                click : function (target) {
                    console.log(target);
                }
            }
        }
    },

    /*--------------------------------------users start------------------------------------*/
    'users': {
        current : false,
        name : 'Users',
        title : 'Users',
        link: 'pages/users/index.html',
        data_table : '',
        filter : 'closed',
        click: function (callback){
            pageCont.load(this.link, function () {

                /*-------------create dataTable---------------*/
                modules['users'].data_table = $('#data-table-users').DataTable({
                    "ajax": {
                        url : "cs?action=users&sub_action=getUsersList",
                        data : {
                            surveyId : 0,
                            userTypeId : 1,
                            search : ''
                        }
                    },
                    "info" : false,
                    "columnDefs": [
                        { targets: '_all', searchable: true, orderable : false}
                    ],
                    "columns": [
                        { "data": "id", "render" : retDataTableRowIndex},
                        { "data": "full_name" /*, title: 'Full name'*/},
                        { "data": "username" /*,  title : 'User name'*/},
                        { "data": "user_type" /*, title : 'User type'*/ },
                        { "data": "user_type_id", "visible" : false},
                        { "data": "country_name", "visible" : false /*, title : 'Country'*/},
                        { "data": "country_id" , "visible" : false},
                        { "data": "survey_id" , "visible" : false},
                        { "data": "first_name" , "visible" : false},
                        { "data": "last_name" , "visible" : false},
                        { "data": "middle_name" , "visible" : false},
                        {"name":"operations", "render":retDataTableRowOperations}
                    ]
                });
                if (callback) callback();
            });
        },
        loadFilter: function (callback){
            $(Constants.MODULE_FILTER_CONTAINER).load('pages/users/filter.html', function () {
                if (callback) callback();
            });
        },
        operations : {
            'new' : {
                click : function (target) {
                    try {

                        var modal = $('#modal-dialog');
                        modal.load('pages/users/user-oper-modal.html', function () {
                            // var user_type = 1;
                            // var country = $("#user_operation #country");
                            // var survey = $("#user_operation #survey");

                            modal.userDefinedModal('open', {
                                /*events : {
                                    'on-open': function () {
                                        /!*user_type.getComboContent('cs?action=common&sub_action=getDataContent&type=getDictionaries', {
                                            typeId : '1001'
                                        });*!/
                                        // country.getComboContent('cs?action=common&sub_action=getDataContent&type=getCountries');

                                     /!*   survey.getComboContent('cs?action=common&sub_action=getDataContent&type=getSurveys', {
                                            typeId : '1000'
                                        });*!/

                                    }
                                },*/
                                buttons: {
                                    '#user_operation_confirm' : function () {
                                        var user = {
                                            userId : 0,
                                            firstName : $('#user_operation #first_name').val(),
                                            lastName : $('#user_operation #last_name').val(),
                                            middleName : $('#user_operation #middle_name').val(),
                                            userType : 1,
                                            username : $('#user_operation #username').val(),
                                            passwrd : $('#user_operation #password').val(),
                                            surveyMember : {
                                                id : 0,
                                                survey : {
                                                    id : 0
                                                },
                                                country : {
                                                    id : 0
                                                    // id : country.find('option:selected').val()
                                                }
                                            }
                                        };
                                        var precont = modal.find('[preloader-container]');
                                        var p = precont.preloader('append', {size:1});
                                        console.log(p);
                                        $.ajax({
                                            type : 'POST',
                                            url : 'cs?action=users&sub_action=addUser',
                                            dataType : 'json',
                                            data : {user : JSON.stringify(user)},
                                            success : function(data){
                                                $.notify2(data);
                                                p.remove();
                                                modal.userDefinedModal('close');
                                                modules.getCurrent().data_table.ajax.reload();
                                            }
                                        })
                                    }
                                }
                            });
                        })
                    } catch(err) {
                        console.error(err);
                    }
                }
            },
            /*---------------------------------new op end---------------------------------------*/
            'edit' : {
                click : function (a) {
                    try {
                        var aa = $(a);
                        var tr = aa.parents('tr[role="row"]');
                        var rowData = modules.getCurrent().data_table.row(tr).data();
                        var modal = $('#modal-dialog');
                        modal.load('pages/users/user-oper-modal.html',  function () {
                            // var user_type = $("#user_operation #user_type");
                            var country = $("#user_operation #country");
                            var survey = $("#user_operation #survey");
                            var first_name = $('#user_operation #first_name');
                            var last_name = $('#user_operation #last_name');
                            var middle_name = $('#user_operation #middle_name');
                            var username = $('#user_operation #username');
                            var password = $('#user_operation #password');
                            modal.userDefinedModal('open', {
                                events:{
                                    'on-open' : function () {

                                       /* user_type.getComboContent('cs?action=common&sub_action=getDataContent&type=getDictionaries', {
                                            typeId : '1001'
                                        }, function () {
                                            user_type.find('option[value="'+ rowData.user_type_id +'"]').attr('selected', 'selected');
                                        });
                                        country.getComboContent('cs?action=common&sub_action=getDataContent&type=getCountries', function () {
                                            country.find('option[value="'+ rowData.country_id +'"]').attr('selected', 'selected');
                                        });

                                        survey.getComboContent('cs?action=common&sub_action=getDataContent&type=getSurveys', {
                                            typeId : '1000'
                                        }, function () {
                                            survey.find('option[value="'+ rowData.survey_id +'"]').attr('selected', 'selected');
                                        });
                                        */
                                        first_name.val(rowData.first_name);
                                        last_name.val(rowData.last_name);
                                        middle_name.val(rowData.middle_name);
                                        username.val(rowData.username);
                                    }
                                },
                                buttons: {
                                    '#user_operation_confirm' : function () {
                                        var user = {
                                            userId : rowData.id,
                                            firstName : first_name.val(),
                                            lastName : last_name.val(),
                                            middleName : middle_name.val(),
                                            //countryId : country.find('option:selected').val(),
                                            userType : 1,//user_type.find('option:selected').val(),
                                            username : username.val(),
                                            passwrd : password.val(),
                                            //survey : {id : survey.find("option:selected").val()}
                                            surveyMember : {
                                                id : 0,
                                                survey : {
                                                    id : 0
                                                },
                                                country : {
                                                    id : 0
                                                    // id : country.find('option:selected').val()
                                                }
                                            }
                                        };

                                        var precont = modal.find('[preloader-container]');
                                        var p = precont.preloader('append', {size:1});

                                        $.ajax({
                                            type : 'POST',
                                            url : 'cs?action=users&sub_action=editUser',
                                            dataType : 'json',
                                            data : {user : JSON.stringify(user)},
                                            success : function(data){
                                                $.notify2(data);
                                                p.remove();
                                                modal.userDefinedModal('close');
                                                modules.getCurrent().data_table.ajax.reload();
                                            }
                                        });
                                    }
                                }
                            });
                        })
                    }
                    catch(err) {
                        console.error(err);
                    }

                }
            },
            'delete' : {
                click : function (a) {
                    var aa = $(a);
                    var tr = aa.parents('tr[role="row"]');
                    var us = modules.getCurrent().data_table.row(tr).data();
                    $.confirm({action: function () {
                        $.ajax({
                            type : 'POST',
                            url : 'cs?action=users&sub_action=deleteUser',
                            dataType : 'json',
                            data : {user_id : us.id},
                            success : function(data){
                                $.notify2(data);
                                modules.getCurrent().data_table.ajax.reload();
                            }
                        });
                    }});
                }
            }
        }
    },
    /*--------------------------------------users end--------------------------------------*/

    /*------------------------------------participants start-------------------------------*/
    'participants': {
        current : false,
        name : 'Participants',
        title : 'Participants',
        link: 'pages/participants/index.html',
        data_table : '',
        click: function (callback){
            pageCont.load(this.link, function () {
                /*-------------create dataTable---------------*/
                modules['participants'].data_table = $('#data-table-participants').DataTable({
                    "ajax": {
                        url : "cs?action=users&sub_action=getUsersList",
                        data : {
                            surveyId : $("#survey_select").find('option:selected').val(),
                            userTypeId : 2,
                            search : ''
                        }
                    },
                    "info" : false,
                    "columnDefs": [
                        { targets: '_all', searchable: true, orderable : false}
                    ],
                    "columns": [
                        { "data": "id", "render" : retDataTableRowIndex},
                        { "data": "full_name", "visible" : false},
                        { "data": "username" /*,  title : 'User name'*/},
                        { "data": "password_original" ,/*, title : 'User type'*/ "visible":false},
                        { "data": "user_type_id", "visible" : false},
                        { "data": "country_name" /*, title : 'Country'*/, "render" : function (data, type, full, meta){
                            return '<img class="circle m-r-5" src="'+ full.country_icon +'"/>' + full.country_name
                        }},
                        { "data": "user_type" /*, title : 'User type'*/},
                        { "data": "country_id" , "visible" : false},
                        { "data": "survey_id" , "visible" : false},
                        { "data": "first_name" , "visible" : false},
                        { "data": "last_name" , "visible" : false},
                        { "data": "middle_name" , "visible" : false},
                        {"name":"operations", "render":retDataTableRowOperations}
                    ]
                });
                if (callback) callback();
            });
        },
        loadFilter: function (callback){
            $(Constants.MODULE_FILTER_CONTAINER).load('pages/participants/filter.html', function () {
                $("#survey_select").getComboContent('cs?action=common&sub_action=getDataContent&type=getSurveys', {}, function (data) {
                    if (callback) callback();
                });
            });
        },
        operations : {
            /*'new' : {
                click : function (target) {
                    try {

                        var modal = $('#modal-dialog');
                        modal.load('pages/participants/participant-oper-modal.html', function () {
                             // var user_type = $("#participant_operation #survey");
                            var country = $("#participant_operation #country");
                            var survey = $("#participant_operation #survey");

                            modal.userDefinedModal('open', {
                                events : {
                                    'on-open': function () {

                                        country.getComboContent('cs?action=common&sub_action=getDataContent&type=getCountries');

                                        survey.getComboContent('cs?action=common&sub_action=getDataContent&type=getSurveys', {
                                            typeId : '1000'
                                        });
                                        survey.append('<option value ="0">'+'None'+'</option>');

                                       /!* survey.on('change', function () {
                                            if ($(this).find('option:selected').val()>0) {
                                                $('#country_div').show();
                                            }
                                            else{
                                                $('#country_div').hide();
                                            }
                                        })*!/
                                    }
                                },
                                buttons: {
                                    '#participation_operation_confirm' : function () {
                                        var user = {
                                            userId : 0,
                                            firstName : $('#participant_operation #first_name').val(),
                                            lastName : $('#participant_operation #last_name').val(),
                                            middleName : $('#participant_operation #middle_name').val(),
                                            userType : 2,//$('#user_operation #user_type').find('option:selected').val(),
                                            username : $('#participant_operation #username').val(),
                                            passwrd : $('#participant_operation #password').val(),
                                            surveyMember : {
                                                id : 0,
                                                survey : {
                                                    id : survey.find('option:selected').val()
                                                },
                                                country : {
                                                    id : country.find('option:selected').val()
                                                }
                                            }
                                        };
                                        var precont = modal.find('[preloader-container]');
                                        var p = precont.preloader('append', {size:1});
                                        console.log(p);
                                        $.ajax({
                                            type : 'POST',
                                            url : 'cs?action=users&sub_action=addUser',
                                            dataType : 'json',
                                            data : {user : JSON.stringify(user)},
                                            success : function(data){
                                                $.notify2(data);
                                                p.remove();
                                                modal.userDefinedModal('close');
                                                modules.getCurrent().data_table.ajax.reload();
                                            }
                                        })
                                    }
                                }
                            });
                        })
                    } catch(err) {
                        console.error(err);
                    }
                }
            },*/
            /*---------------------------------new op end---------------------------------------*/
            'edit' : {
                click : function (a) {
                    try {
                        var aa = $(a);
                        var tr = aa.parents('tr[role="row"]');
                        var rowData = modules.getCurrent().data_table.row(tr).data();
                        var modal = $('#modal-dialog');
                        modal.load('pages/participants/participant-oper-modal.html',  function () {
                            var user_type = $("#participant_operation #user_type");
                            var country = $("#participant_operation #country");
                            var survey = $("#participant_operation #survey");
                            var first_name = $('#participant_operation #first_name');
                            var last_name = $('#participant_operation #last_name');
                            var middle_name = $('#participant_operation #middle_name');
                            var username = $('#participant_operation #username');
                            var password = $('#participant_operation #password');
                            modal.userDefinedModal('open', {
                                events:{
                                    'on-open' : function () {

                                        country.getComboContent('cs?action=common&sub_action=getDataContent&type=getCountries', function () {
                                            country.find('option[value="'+ rowData.survey_id +'"]').attr('selected', 'selected');

                                        });
                                        survey.getComboContent('cs?action=common&sub_action=getDataContent&type=getSurveys', {
                                            typeId : '1000'
                                        }, function () {
                                            survey.find('option[value="'+ rowData.survey_id +'"]').attr('selected', 'selected');
                                        });
                                        first_name.val(rowData.first_name);
                                        last_name.val(rowData.last_name);
                                        middle_name.val(rowData.middle_name);
                                        username.val(rowData.username);
                                    }
                                },
                                buttons: {
                                    '#participation_operation_confirm' : function () {
                                        var user = {
                                            userId : rowData.id,
                                            firstName : first_name.val(),
                                            lastName : last_name.val(),
                                            middleName : middle_name.val(),
                                            userType : 2,//user_type.find('option:selected').val(),
                                            username : username.val(),
                                            passwrd : password.val(),
                                            surveyMember : {
                                                id : 0,
                                                survey : {
                                                    id : survey.find("option:selected").val()
                                                },
                                                country : {
                                                    id : country.find('option:selected').val()
                                                }
                                            }
                                        };

                                        var precont = modal.find('[preloader-container]');
                                        var p = precont.preloader('append', {size:1});

                                        $.ajax({
                                            type : 'POST',
                                            url : 'cs?action=users&sub_action=editUser',
                                            dataType : 'json',
                                            data : {user : JSON.stringify(user)},
                                            success : function(data){
                                                $.notify2(data);
                                                p.remove();
                                                modal.userDefinedModal('close');
                                                modules.getCurrent().data_table.ajax.reload();
                                            }
                                        });
                                    }
                                }
                            });
                        })
                    }
                    catch(err) {
                        console.error(err);
                    }
                }
            },
            'reset-password' : {
                click : function (a) {
                    var aa = $(a);
                    var tr = aa.parents('tr[role="row"]');
                    var rowData = modules.getCurrent().data_table.row(tr).data();
                    $.ajax({
                        type : 'POST',
                        url : 'cs?action=users&sub_action=resetPassword',
                        dataType : 'json',
                        data : {user_id : rowData.id},
                        success : function(data){
                            $.notify2(data);
                            modules.getCurrent().data_table.ajax.reload();
                        }
                    });
                }

            },
            'print' : {
                title  : 'Print participant card',
                click : function (a) {
                    var aa = $(a);
                    var tr = aa.parents('tr[role="row"]');
                    var rowData = modules.getCurrent().data_table.row(tr).data();
                    var w = window.open("print-participant-card?type=1&user_id=" + rowData.id, "Print participant card","width=500,height=500");
                }
            },
            'print-all' : {
                title  : 'Print all participants card of current survey',
                click : function () {
                    var list = $("#survey_select").find('option[activation="1"]');
                    if(list.length > 0){
                        var w = window.open("print-participant-card?type=2&survey_id=" + list.val(), "Print participants card","width=500,height=900");
                    } else {
                        $.notify1("There are not active survey to print cards", messagesTypes.ERROR);
                    }
                }
            },
            'delete' : {
                click : function (a) {
                    var aa = $(a);
                    var tr = aa.parents('tr[role="row"]');
                    var us = modules.getCurrent().data_table.row(tr).data();
                    $.confirm({action: function () {
                        $.ajax({
                            type : 'POST',
                            url : 'cs?action=users&sub_action=deleteUser',
                            dataType : 'json',
                            data : {user_id : us.id},
                            success : function(data){
                                $.notify2(data);
                                modules.getCurrent().data_table.ajax.reload();
                            }
                        });
                    }});
                }
            }
        }
    },

    /*-------------------------questionnaires start------------------------------*/
    'questionnaires': {
        current : false,
        name: 'Questionnaires',
        title: 'Questionnaires',
        /*filter : 'closed',*/
        link: 'pages/questionnaires/index.html',
        click: function (callback){
            pageCont.load(this.link + '?current_timestamp_for_cache=' + new Date().getTime(), function () {
                modules['questionnaires'].data_table = $('#data-table-questionnaire').DataTable({
                    "ajax":{
                        url : "cs?action=common&sub_action=getDataContent&type=getQuestionnaires",
                        data : {typeId: '0', privacyType : '0', startDate : '',
                                survey_id : $('#questionnaires_filter #survey_select').find('option:selected').val()}
                    },
                    "info" : false,
                    "bFilter" : false,
                    "columnDefs": [
                        { targets: '_all', searchable: true, orderable : false}
                    ],
                    "columns" :[
                        {"name": "#", "render" : retDataTableRowIndex},
                        {"name": "content", "data": "content"},
                        {"name": "privacy type" ,"data": "privacy_name"},
                        {"name": "type", "data": "type_name"},
                        {"name": "Time interval", "data": "time_interval"},
                        {"name": "questionnaire_movement_id", "data": "questionnaire_movement_id", "visible" : false},
                        {"name":"operations", "render":retDataTableRowOperations}
                    ]
                });
                if (callback) callback();
            });
        },
        loadFilter: function (callback){
            $(Constants.MODULE_FILTER_CONTAINER).load('pages/questionnaires/filter.html', function () {
                var survey_select = $('#questionnaires_filter #survey_select');
                survey_select.getComboContent('cs?action=common&sub_action=getDataContent&type=getSurveys', {}, function (data) {
                    survey_select.prepend('<option selected value ="-1">'+'None'+'</option>');
                    survey_select.selectpicker('refresh');
                    if (callback) callback();
                });
            });
        },

        operations : {
            'new' : {
                title : 'Create new questionnaire',
                click : function (target) {
                    try{
                        var modal = $('#modal-dialog');
                        modal.load('pages/questionnaires/quest-oper-modal.html', function () {
                            questionnaire_operation_new_modal(modal);
                        });
                    }
                    catch(err) {
                        console.log(err);
                    }
                }
            },
            'view' : {
                title : 'View questionnaire',
                'inc-type' : 'table-data', // element,
                'click' : function (row_data) {
                    try{
                        $("[data-ma-action='filter-close']").click();

                        viewQuestionnaire(row_data.id);

                    }catch (err){
                        console.log(err)
                    }
                }
            },
            'delete' : {
                title : 'Detele questionnaire',
                click : function (a) {
                    var aa = $(a);
                    var tr = aa.parents('tr[role="row"]');
                    var row_data = modules.getCurrent().data_table.row(tr).data();
                    $.confirm({action: function () {
                        deleteQuestionnaire(row_data.id, function (data) {
                            $.notify2(data);
                            modules.getCurrent().data_table.ajax.reload();
                        });
                    }});
                }
            },
            'send' : {
                title : 'Send questionnaire to active survey',
                /*'inc-type' : 'table-data',*/
                'click' : function (a) {

                    $("[data-ma-action='filter-close']").click();

                    var aa = $(a);
                    var tr = aa.parents('tr[role="row"]');
                    var rowData = modules.getCurrent().data_table.row(tr).data();

                    viewQuestionnaire(rowData.id, function (questionnaire) {

                        $("#send-button").addClass('disabled');

                        startQuestionnaire(questionnaire.id, function (data) {
                            $.closeNotify();
                            $.notify2(data);
                            console.log(data);
                            if (data.resultMessage.resultStatus == 'SUCCESS'){

                                //modules.getCurrent().data_table.ajax.reload();

                                questionnaire.questionnaireMovementId = data.data;

                                $.closeNotify();
                                $.notifyWaiting('Sending questionnaire to clients ...');
                                $.socket('send', { action : 'BROADCAST_QUESTION', body : questionnaire});
                            }
                        })
                    });
                }
            },

        }
    },

    /*----------------------------------------------reports start-------------------------------------------------*/
    'reports':{
        current : false,
        name : 'Reports',
        title : 'Reports',
        link : 'pages/reports/index.html',
        data_table : '',
        click : function (callback) {
            pageCont.load(this.link, function () {
                modules['reports'].data_table = $('#data-table-report').DataTable({
                    "ajax": {
                        url: "cs?action=common&sub_action=getDataContent&type=getReportBySurvey",
                        data: {survey_id: $("#survey_id").find("option:selected").val()}
                    },
                    "info": false,
                    "bFilter": false,
                    "columnDefs": [
                        {targets: '_all', searchable: true, orderable: false}
                    ],
                    "columns": [
                        {"name": "#", "render": retDataTableRowIndex},
                        {"name": "id", "data": "id", "visible": false},
                        {"name": "date", "data": "start_date"},
                        {"name": "vote details", "data": "content"},
                        {"name": "winner_count", "data": "winner_count"},
                        {"name": "vote result", "data": "result_variant", "visible": false},
                        {"name": "operations", "render": retDataTableRowOperations}
                    ]
                });

                var data = modules['reports'].data_table.rows().data();

                if(callback) callback();
            })
        },
        loadFilter : function (callback) {
            $(Constants.MODULE_FILTER_CONTAINER).load('pages/reports/filter.html', function () {
                $('#survey_id').getComboContent('cs?action=common&sub_action=getDataContent&type=getSurveys',{}, function (data) {
                    if (callback) callback();
                });
            });
        }
        ,
        operations : {
            'interactive-report' : {
                click : function () {
                    window.open("report-interactive", "Report by Questionnaire","width=500,height=800");
                }
            },
            'view' : {
                click : function (a) {
                    var aa = $(a);
                    var tr = aa.parents('tr[role="row"]');
                    var rowData = modules.getCurrent().data_table.row(tr).data();
                    var id = rowData.id;

                   window.open("report-by-questionnaire?questionaire_movement_id=" + id, "Report by Questionnaire");
                }
            }
        }
    }
};

$(function () {

    var module_cont = $(Constants.MODULE_MENU_CONTAINER);

    module_cont.find("li").find("a[module]").click(function () {
        var th = $(this);
        module_cont.find("li").removeClass('active');
        th.parent('li').addClass('active');

        var current_module_id = th.attr('module');

        $.pushState({url : '?module=' + current_module_id});

        var current_module =  modules[current_module_id];

        if (current_module){

            modules.setCurrent(current_module_id);
            if ( current_module.filter == 'closed' ){
                $("[data-ma-action='filter-close']").click();
            } else{
                $("[data-ma-action='filter-open']").click();
            }

            $(Constants.MODULE_TITLE_CONTAINER).html( current_module.title != undefined ? current_module.title : th.html() );

            /*------------- Append operation buttons ------------*/
            var operationCont = $(Constants.MODULE_OPEARTION_CONTAINER);
            operationCont.html('');
            if (current_module.operations){
                $.each(Object.keys(current_module.operations), function (index, key){
                    var obj = operations[key];
                    if ( obj.type == operations.BUTTTON_TYPE_OUTER ){
                        var attrs = '';
                        $.each(obj.attributes, function (i, v) {
                            attrs += v.attr + '="' + v.value + '" '
                        });

                        var title = (current_module.operations[key].title != undefined) ?  current_module.operations[key].title : obj.title;
                        var button = $('<a '+ attrs +' title="'+ title +'" href="javascript:void(0)">' +
                            obj.icon + //'<i class="'+ obj.class +' font-size25"></i>' +
                            '</a>');
                        button.on('click', function () {
                            $.pushState({url : '?module=' + current_module_id + '&operation=' + key});
                            current_module.operations[key].click(this);
                        });
                        var li = $("<li></li>").append(button);
                        operationCont.append(button);
                    }
                });
            }
            /*------------- Append operation buttons ------------*/

            pageCont.html('<div class="content-loader" style="">' +
                '<div class="preloader pls-blue">' +
                '<svg class="pl-circular" viewBox="25 25 50 50">' +
                '<circle class="plc-path" cx="50" cy="50" r="20"></circle>' +
                '</svg>' +
                '<p>Please wait...</p>' +
                '</div>' +
                '</div>');

            if (current_module.loadFilter){
                current_module.loadFilter(function(){
                    current_module.click();
                })
            }else {
                current_module.click(function (data) {
                    $(document).trigger("evt_module_click_callback", [current_module_id, data]);
                });
            }
        }
    });


    var options = $.url_parse();
    if (options.module){
        module_cont.find("a[module='"+ options.module +"']").click();
        if(options.operation){
            /*$(document).on('evt_module_click_callback_' + options.module, {}, function (current_module_id, data) {
                var x = modules[options.module].operations[options['operation']];
                x.click(options);
            });*/
        }
    }else {
        $.url_parse();
        module_cont.find("li:first").find("a[module]").click();
    }

    $(document).on('click', 'a[inner-operations]', function (event) {
        var a = $(this);
        var op = a.attr('current-inner-oper');
        var mod_oper = modules.getCurrent().operations[op];
        var param = undefined;
        if (mod_oper['inc-type'] == 'table-data'){
            var aa = $(a);
            var tr = aa.parents('tr[role="row"]');
            param = modules.getCurrent().data_table.row(tr).data();
            $.pushState({url : '?module=' + modules.current_module + '&operation=' + op + '&' + $.json2Url(param), post : param});
        } else {
            param = a;
        }
        mod_oper.click(param);
    });
});

window.onpopstate = function(event) {
    console.log('window.onpopstate');
    console.log(event);
    /*if (event.state){

    }*/
    var options = $.url_parse();
    console.log(options.module);
    if (options.module){
        modules[options.module].click(function () {
            modules[options.module].operations[options['operation']].click(history.state);
        });
    }else {
        $(Constants.MODULE_MENU_CONTAINER).find("li:first").find("a[module]").click();
    }
};