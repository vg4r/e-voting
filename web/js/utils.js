retDataTableRowIndex = function ( data, type, full, meta ) {
    return meta.row + 1;
};
retDataTableRowInput = function ( data, type, full, meta ) {
    return '<input data-table-inner-input class="none w-100 h-100-pers" type="text" style="" placeholder="Type here..."/>';
};

retDataTableRowOperations = function (data, type, full, meta) {

    var  current_module = modules.getCurrent();
    var ul = '<ul class="pull-right actions">';
    $.each(Object.keys(current_module.operations), function (index, key){
        var obj = operations[key];
        if ( obj.type == operations.BUTTTON_TYPE_INNER ){
            var attrs = '';
            $.each(obj.attributes, function (i, v) {
                attrs += v.attr + '="' + v.value + '" '
            });
            var title = (current_module.operations[key].title != undefined) ?  current_module.operations[key].title : obj.title;
            ul += '<li><a title="'+ title +'" '+ attrs +' inner-operations current-inner-oper="'+ key +'" href="javascript:void(0)">' +
                obj.icon + //'<i class="'+ obj.class +'"></i>' +
                '</a></li>';
            /*
            button.on('click', function () {
                current_module.operations[key].click(this);
            });

            var li = $("<li></li>").append(button);
            */
        }
    });
    ul += '</ul>';
    return ul;
};

$.pushState = function(options){
    history.pushState(options.post, null, options.url);
};

$.url_parse = function() {
    var arr = window.location.href.split("?");
    var parameter = {};
    if (arr[1]){
        parameter = $.url2json(arr[1]);
    }
    return parameter;
};

$.json2Url = function (json) {
    var url = '';
    Object.keys(json).forEach(function (value){
        url += value + '=' + encodeURI(json[value]) + '&';
    });
    return url;
};

$.url2json = function (url) {
    var parameter = {};
    var arr2 =  url.trim().split("&");
    arr2.forEach(function (item2, index2){
        var item3 = item2.trim().split("=");
        parameter[item3[0]] = decodeURI(item3[1]);
    });
    return parameter;
};

var messagesTypes = {
    'SUCCESS' : 'success',
    'ERROR' : 'danger',
    'WARNING' : 'warning',
    'INVERSE' : 'inverse'
};

var messagesStyles = {
  'style1' : {from: "bottom", align: "left"},
  'style2' : {from: "top", align: "right"}
};

$.notify1 = function(message, type , styles, delay) {
    console.log('$.notify1 message = ' + message + ", type = " + type + ", styles = " + styles + ", delay = " + delay);
    if (!styles){
        styles= messagesStyles.style1
    };

    var tt = $.growl({message: message}, {
        type: type,
        allow_dismiss: !1,
        label: "Cancel",
        className: "btn-xs btn-inverse",
        placement: styles,
        delay: delay,
        animate: {enter: "animated fadeInUp", exit: "animated fadeOutDown"},
        offset: {x: 30, y: 30}
    });

    $(document).on('remove-all-notifications', function () {
        tt.close();
    });

    return tt;
};

$.notify2 = function(obj, delay) {
    console.log('$.notify2' + JSON.stringify(obj));
    var ss = $.notify1(obj.resultMessage.message.message, messagesTypes[obj.resultMessage.resultStatus], undefined, delay);
    return ss;
};

$.closeNotify = function(){

    $(document).trigger('remove-all-notifications');
    //gro.close();
};

$.notifyWaiting = function(message){
    var ss = $.notify1(message, messagesTypes['INVERSE'], undefined, -1);
    return ss;
};


$.confirm = function (options) {
    var op = $.extend({
        title : "Are you sure?",
        text : "You will not be able to recover this item",
        type : "warning",
        cancelButton : true,
        confirmButtonText : "Yes, delete it!",
        action : function () {
            alert("noting to do");
        }

    }, options);
    swal({
        title: op.title,
        text: op.text,
        type: op.type,
        showCancelButton: op.cancelButton,
        confirmButtonText: op.confirmButtonText
    }).then(function(){
        op.action();
    });
};

String.prototype.toTimeFormat = function (pattern) {
    if (!pattern) pattern = 'hh:mm:ss';
    var sec_num = parseInt(this, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours == 0){pattern = 'mm:ss'}else if(hours > 0 && hours   < 10 ) {hours   = '0' + hours;}
    if (minutes < 10) {minutes = '0' + minutes;}
    if (seconds < 10) {seconds = '0' + seconds;}
    return pattern.replace('hh', hours).replace('mm', minutes).replace('ss', seconds);

};

var ConstantWords = {
    Allocate : "Allocate a total of # votes across the answers",
    Allocate2 : "Allocate a total of # votes below. Then click confirm",
    Allocate3 : "Allocate up to # votes, no more than 6 per candidate",
    SelectYourAnswer : "Select your answer or split your vote",
    YouShouldSelectI : "You should select individual # votes",
    YouShouldSelectA : "You should select at least one answer",
    YouShouldSelectU : "You should select up to # votes",
    Confirm0 : "Confirm 0 votes",
    Confirm : "Confirm",
    ConfirmXvotes : "Confirm # votes",
    Votes2Alloc : "Votes to allocate ",
    PleaseWaitVE : "Please wait until the voting ends..",
    Back2NormalVote : "Back to normal vote",
    SplitVote : "Split vote",
    Back2Voting : "Back to voting"
};