var questionnaireTypeProperties = {
    '100': {
        ok : function (vote, maxVote) { //mode = 0 - splied, 1 - not splited,
            if(vote == maxVote){
                return true;
            } else{
                var mode = $("#questionnaire-variant-container").attr("data-type-style");
                this.error = this.errors[mode].replace('#', maxVote)
            }
        },
        note  : [ConstantWords.Allocate, ConstantWords.SelectYourAnswer],
        errors : [ConstantWords.YouShouldSelectI, ConstantWords.YouShouldSelectA],
        error : ''
    },
    '101': {
        ok : function (vote, maxVote) {
            if(vote == maxVote){
                return true;
            } else {
                this.error = this.errors[0].replace('#', maxVote)
            }
        },
        note : [ConstantWords.Allocate2],
        errors : [ConstantWords.YouShouldSelectI],
        error : ''
    },
    '102': {
        ok :function(vote, maxVote){
            if(vote <= maxVote){
                return true;
            } else {
                this.error = this.errors[0].replace('#', maxVote);
            }
        },
        note : [ConstantWords.Allocate3],
        errors : [ConstantWords.YouShouldSelectU],
        error : ''
    }
};

function addQuestionnaire(questionnaire, callback) {
    $.ajax({
        type : 'POST',
        url : 'cs?action=questionnaire&sub_action=addQuestionnaire',
        dataType : 'json',
        data : { questionnaire : JSON.stringify(questionnaire)},
        success : function(data){
            if (callback){
                callback(data);
            }
        }
    })
}


function startQuestionnaire(questionnaire_id, callback) {
    $.ajax({
        type : 'POST',
        url : 'cs?action=questionnaire&sub_action=startQuestionnaire',
        dataType : 'json',
        data : { questionnaire_id : questionnaire_id},
        success : function(data){
            if (callback){
                callback(data);
            }
        }
    })
}

function deleteQuestionnaire(id , callback) {
    $.ajax({
        type : 'POST',
        url : 'cs?action=questionnaire&sub_action=deleteQuestionnaire',
        dataType : 'json',
        data : {id : id},
        success : function(data){
            if(callback){
                callback(data)
            }
        }
    });
}


function answerQuestionnaire(questionnaire_movement_id,questionnaire_answers , callback) {
    $.ajax({
        type : 'POST',
        url : 'cs?action=questionnaire&sub_action=answerQuestionnaire',
        dataType : 'json',
        data : { questionnaire_movement_id : questionnaire_movement_id, questionnaire_answers : JSON.stringify(questionnaire_answers)},
        success : function(data){
            $.notify2(data);
            if (callback){
                callback(data);
            }
        }
    })
}

function questionnaire_varaints_delete_form_table(a) {
    var tr = $(a).parents('tr[role="row"]');

    var table = $('#data-table-questionnaire-variants').DataTable();
    table.row(tr).remove().draw();
}

function questionnaire_operation_new_modal(modal){
    //var survey = $('#questionnaire_operation #survey-questionnaire_operation');
    var quest_type = $('#questionnaire_operation #question_type');
    var variant_count = $('#questionnaire_operation #variant_count');
    var privacy_type = $('#questionnaire_operation #privacy_type');
    var answer_variant = $('#questionnaire_operation #variants');
    var vote_count = $('#questionnaire_operation #vote_count');
    var winner_count = $('#questionnaire_operation #winner_count');
    modal.userDefinedModal('open', {
        events : {
            'on-open': function () {
                quest_type.on('change', function () {
                    var type = quest_type.find('option:selected').val();
                    if (type == '100') {
                        answer_variant.hide();
                        vote_count.val('6');
                        vote_count.attr('disabled', 'disabled');
                        winner_count.val('1');
                        winner_count.attr('disabled', 'disabled');

                        variant_count.val('3');
                        variant_count.attr('disabled', 'disabled');
                    } else if (type == '101'){
                        answer_variant.show();
                        vote_count.val('6');
                        vote_count.attr('disabled', 'disabled');

                        winner_count.val('1');
                        winner_count.attr('disabled', 'disabled');

                        variant_count.val('');
                        variant_count.removeAttr('disabled');

                    } else if (type == '102'){
                        answer_variant.show();
                        vote_count.removeAttr('disabled');

                        winner_count.val('');
                        winner_count.removeAttr('disabled');

                        variant_count.val('');
                        variant_count.removeAttr('disabled');
                    }
                });
                /*$().getComboContent('cs?action=common&sub_action=getDataContent&type=getSurveys', {}, function (data) {
                    $.each(data, function (i, s) {
                        if (s.end_date.trim().length == 0){
                            survey.append('<option id="'+ s.id +'">'+ s.name +'</option>');
                        }
                    });
                    survey.prepend('<option selected id="-1">None</option>');
                    survey.selectpicker('refresh');
                });*/

                quest_type.getComboContent( 'cs?action=common&sub_action=getDataContent&type=getDictionaries',{
                    typeId: '1003'
                }, function () {
                    quest_type.change();
                });

                privacy_type.getComboContent( 'cs?action=common&sub_action=getDataContent&type=getDictionaries',{
                    typeId: '1002'
                });

                var data_table = $("#data-table-questionnaire-variants").DataTable({
                    "searching": false,
                    "ordering":  false,
                    "paging": false,
                    "info": false,
                    "destroy" : true,
                    "columns" : [
                        { "name": "#", "render" : retDataTableRowIndex},
                        { "name": "id", "data": "id", "visible" : false},
                        { "name": "Variant", "data": "variant", "render": retDataTableRowInput},
                        { "name": "operations", "render": function ( data, type, full, meta ) {
                            return '<a class="btn btn-default" href="javascript:void(0)" onclick="questionnaire_varaints_delete_form_table(this)"><i class="zmdi zmdi-minus"></i></a>';
                        }
                        }
                    ]
                });
                variant_count.focusout(function(){
                    var count = parseInt($(this).val());
                    var len = data_table.rows().data().length;
                    var x = count - len;
                    if( x > 0){
                        for (var i = 0; i < x; i++){
                            data_table.row.add({
                                "id": 0
                            })
                        }
                        data_table.draw( false );
                    } else if( x < 0) {
                        for (var i = len; i > (len - count) + 1; i--){
                            data_table.row(i).remove();
                        }
                        data_table.draw( false );
                    }
                });
            },
            'on-close' : function () {
                $("#data-table-questionnaire-variants").dataTable().destroy();
            }
        },
        buttons : {
            /*-------------add button--------------*/
            '#questionnaire_operation_confirm' : function () {

                /*var c = $('.chosen-choices').val(),

                    d = '[', k;
                $.each(c, function(i, val){
                    if(i!=0){
                        d+=',';
                    }
                    d += ''+val+'}';
                });

                d += ']';*/
                var questionnaireVariants = [];
                $("#data-table-questionnaire-variants").find('[data-table-inner-input]').each(function () {
                    var element = $(this);
                    questionnaireVariants.push({
                        content : element.val()
                    })
                });
                var survey_questionnaire_operation = $("#survey-questionnaire_operation");
                var questionnaire = {
                    id : 0,
                    typeId : quest_type.find('option:selected').val(),
                    privacyType : privacy_type.find('option:selected').val(),
                    content : $('#questionnaire_operation #content_text').val(),
                    timeInterval : $('#questionnaire_operation #time_interval').val(),
                    startDate : '12/02/2017',
                    surveyId : 0,//survey_questionnaire_operation.find('option:selected').val(),
                    questionnaireVariants : questionnaireVariants,
                    voteCount : vote_count.val(),
                    winnerCount : winner_count.val() == "" ? 1 : winner_count.val()
                };

                var precont = modal.find('[preloader-container]');
                var p = precont.preloader('append', {size:1});

                /*-----------------------ajax for add-------------------------*/
                addQuestionnaire(questionnaire, function (data) {
                    $.notify2(data);
                    p.remove();
                    modal.userDefinedModal('close');
                    modules.getCurrent().data_table.ajax.reload();
                });
            }
            /*----------------------------------add button end-------------------------------------------*/
        }
        /*----------------------------------------buttons-------------------------------------------------*/
    })
};


function viewQuestionnaire(questionnaireId, callback) {
    $('#container').load('pages/questionnaires/view.html', function () {
        var questionnaire_variant_container1 = $("#questionnaire-variant-container");

        $.ajax({
            url : 'cs?action=common&sub_action=getDataContent&type=getQuestionnaireDetail2&id=' + questionnaireId,
            dataType :  'json',
            success : function (data) {
                console.log(data);
                var d1 = data[0].data[0];
                var questionnaireVariantsArray = [];
                $.each(data[1].data, function (index, value) {
                    questionnaireVariantsArray.push({
                        id : value.id,
                        content : value.content,
                        questId : value.quest_id,
                        relId : value.rel_id,
                        typeId : 'c'
                    });
                });

                var questionnaire = {
                    id : d1.id,
                    typeId : d1.type_id,
                    privacyType : d1.privacy_type_id,
                    content : d1.content,
                    timeInterval : d1.time_interval,
                    voteCount : d1.vote_count,

                    questionnaireVariants : questionnaireVariantsArray
                };

                var questionnaire_content = $("#questionnaire-content");

                questionnaire_content.data('questionnaire', questionnaire);


                questionnaire_content.html(questionnaire.content);

                var  style = 0; // splited
                var html = '';
                if (questionnaire.typeId == 100){
                    $.each(questionnaire.questionnaireVariants, function (index, value){
                        html += '<div class="col-md-12 m-b-10"><button class="btn btn-lg btn-success m-r-10 vote-button text-white w-100">'+ value.content +'</button></div>'
                    });

                }else {
                    $.each(questionnaire.questionnaireVariants, function (index, value){
                        html += '<div class="row m-b-10 m-t-10">' +
                            '<div class="col-md-3 text-center p-10">' +
                            value.content +
                            '</div>' +
                            '<div questionnaire-variat="'+ value.id +'" class="col-md-9 text-center">' +
                            '<button action-button vote-value="1" class="btn btn-lg btn-success m-r-10 vote-button text-white">1</button>' +
                            '<button action-button vote-value="2" class="btn btn-lg btn-success m-r-10 vote-button text-white">2</button>' +
                            '<button action-button vote-value="3" class="btn btn-lg btn-success m-r-10 vote-button text-white">3</button>' +
                            '<button action-button vote-value="4" class="btn btn-lg btn-success m-r-10 vote-button text-white">4</button>' +
                            '<button action-button vote-value="5" class="btn btn-lg btn-success m-r-10 vote-button text-white">5</button>' +
                            '<button action-button vote-value="6" class="btn btn-lg btn-success m-r-10 vote-button text-white">6</button>' +
                            '</div>' +
                            '</div>';
                    });
                }

                $("#questionnaire-vote-note").html(questionnaireTypeProperties[questionnaire.typeId].note[style].replace("#", questionnaire.voteCount));
                $("#votes-2-allocate").html( ConstantWords.Votes2Alloc + questionnaire.voteCount);
                $("#questionnaire-variant-container").find('#container-1-inner').html(html);

                if (callback){
                    callback(questionnaire);
                }
            }
        });

    });
}


function send2ClientOnclick() {
    var questionnaire = $("#questionnaire-content").data("questionnaire");
    $("#send-button").addClass('disabled');

    startQuestionnaire(questionnaire.id, function (data) {
        $.closeNotify();
        $.notify2(data);
        console.log(data);
        if (data.resultMessage.resultStatus == 'SUCCESS'){

            questionnaire.questionnaireMovementId = data.data;

            $.closeNotify();
            $.notifyWaiting('Sending questionnaire to clients ...');
            $.socket('send', { action : 'BROADCAST_QUESTION', body : questionnaire});
        }
    })
}