package com.mv.service;

import com.mv.dao.UserDao;
import com.mv.dao.postgres.UserDaoPostgresImpl;
import com.mv.domain.User;
import com.mv.domain.exceptions.UnrecognizedOperationException;
import com.mv.domain.exceptions.UserDefinedException;
import com.mv.utils.Constants;

public class FactoryUsersOperations {

    public static UserDao getUsersOperationsProvider(User loggedUser , Constants.DB_PROVIDER dataSourceProvider)throws UserDefinedException {
        if (dataSourceProvider.equals(Constants.DB_PROVIDER.POSTGRESQL)) {
            return new UserDaoPostgresImpl(loggedUser);
        }

        throw new UnrecognizedOperationException();
    }



}
