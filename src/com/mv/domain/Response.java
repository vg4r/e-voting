package com.mv.domain;

import com.mv.domain.exceptions.UnrecognizedOperationException;
import com.mv.domain.exceptions.UserDefinedException;
import com.mv.utils.Constants;
import javax.servlet.http.HttpServletResponse;

public class Response<T> extends AbstractObject{

    private ResultMessage resultMessage;
    private T data;


    public Response() {

        Message message = new Message("", "");

        setResultMessage(new ResultMessage(Constants.ResultStatus.SUCCESS , message));
    }

    public Response(String jsonString) {

        super();

    }

    public void initFrom(UserDefinedException userDefinedException) {

        ResultMessage resultMessage = new ResultMessage(Constants.ResultStatus.ERROR, new Message(userDefinedException.getErrorCode(), userDefinedException.getMessage() ));
        setResultMessage(resultMessage);



    }

    public ResultMessage getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(ResultMessage resultMessage) {
        this.resultMessage = resultMessage;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void writeToHttpResponseHeader(HttpServletResponse httpServletResponse){
        httpServletResponse.addHeader(Constants.HEADER_RESULT_STATUS, resultMessage.getResultStatus().name());
        httpServletResponse.addHeader(Constants.HEADER_RESULT_CODE, resultMessage.getMessage().getCode());
        httpServletResponse.addHeader(Constants.HEADER_RESULT_MESSAGE, resultMessage.getMessage().getMessage());
    }


    /*public void writeTo(OutputStream outputStream){
        try{
            String jsonString = getJsonString();
            outputStream.write(jsonString.getBytes());
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/


}
