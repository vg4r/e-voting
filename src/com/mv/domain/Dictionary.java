package com.mv.domain;

/**
 * Created by SuleymanovVA on 6/10/2017.
 */
public class Dictionary {

    private int id;
    private int typeId;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String  name) {
        this.name = name;
    }
}
