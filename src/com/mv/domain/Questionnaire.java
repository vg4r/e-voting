package com.mv.domain;

import java.util.Date;

public class Questionnaire {
    private Integer id;
    private long questionnaireMovementId;
    private Integer typeId;
    private Integer privacyType;
    private String content;
    private Integer timeInterval;
    private String startDate;
    private String[] variants;
    private Integer surveyId;
    private Integer voteCount;
    private Integer winnerCount;

    private Date broadCastTime;
    private QuestionnaireVariant[] questionnaireVariants;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public long getQuestionnaireMovementId() {
        return questionnaireMovementId;
    }

    public void setQuestionnaireMovementId(long questionnaireMovementId) {
        this.questionnaireMovementId = questionnaireMovementId;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getPrivacyType() {
        return privacyType;
    }

    public void setPrivacyType(Integer privacyType) {
        this.privacyType = privacyType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(Integer timeInterval) {
        this.timeInterval = timeInterval;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String[] getVariants() {
        return variants;
    }

    public void setVariants(String[] variants) {
        this.variants = variants;
    }

    public Integer getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Integer surveyId) {
        this.surveyId = surveyId;
    }

    public Integer getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(Integer voteCount) {
        this.voteCount = voteCount;
    }

    public Date getBroadCastTime() {
        return broadCastTime;
    }

    public void setBroadCastTime(Date broadCastTime) {
        this.broadCastTime = broadCastTime;
    }

    public QuestionnaireVariant[] getQuestionnaireVariants() {
        return questionnaireVariants;
    }

    public void setQuestionnaireVariants(QuestionnaireVariant[] questionnaireVariants) {
        this.questionnaireVariants = questionnaireVariants;
    }

    public Integer getWinnerCount() {
        return winnerCount;
    }

    public void setWinnerCount(Integer winnerCount) {
        this.winnerCount = winnerCount;
    }
}
