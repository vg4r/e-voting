package com.mv.domain.exceptions;

public class AuthenticationException extends UserDefinedException{

    public AuthenticationException(){
        super(20501, "UnAuth");
    }
}
