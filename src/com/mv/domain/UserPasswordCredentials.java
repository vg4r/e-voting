package com.mv.domain;

public class UserPasswordCredentials implements Credentials {
    private String userName;
    private String pwdHash;


    public UserPasswordCredentials() {
        super();
    }

    public UserPasswordCredentials(String userName, String pwdHash) {
        this.userName = userName;
        this.pwdHash = pwdHash;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPwdHash() {
        return pwdHash;
    }

    public void setPwdHash(String pwdHash) {
        this.pwdHash = pwdHash;
    }
}
