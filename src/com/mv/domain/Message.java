package com.mv.domain;

public class Message {
    private String code;
    private String message;

    public Message() {

    }

    public Message(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public Message(int code, String message) {
        this.code = String.valueOf(code);
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
