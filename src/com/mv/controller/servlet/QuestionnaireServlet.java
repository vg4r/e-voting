package com.mv.controller.servlet;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mv.dao.QuestionnaireDao;
import com.mv.domain.*;
import com.mv.domain.exceptions.AuthenticationException;
import com.mv.domain.exceptions.UserDefinedException;
import com.mv.service.FactoryQuestionnaireOperations;
import com.mv.utils.Constants;
import com.mv.utils.Utils;
import com.mv.utils.logger.FactoryLogger;
import com.mv.utils.logger.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;


@WebServlet(name = "QuestionnaireServlet")
public class
QuestionnaireServlet extends HttpServlet{
    Logger logger = FactoryLogger.getLogger();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        response.setContentType("application/json");
        OutputStream outputStream = null;
        try {
            outputStream = response.getOutputStream();
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute(Constants.LOGGED_USER);
            if(user == null) throw new AuthenticationException();

            String subAction = request.getParameter(Constants.SUB_ACTION);
            logger.trace("subact="+subAction);
            if(subAction.equalsIgnoreCase(Constants.SUB_ACTIONS.addQuestionnaire.name())){
                QuestionnaireDao questionnaireDao = FactoryQuestionnaireOperations.getQuestionnaireOperationsProvider(user, Constants.DB_PROVIDER.POSTGRESQL);
                String json = request.getParameter("questionnaire");
                logger.trace(json);
                ObjectMapper mapper = new ObjectMapper();
                Questionnaire questionnaire = mapper.readValue(json, Questionnaire.class);
                logger.trace();
                Boolean bool = questionnaireDao.addQuestionnaire(questionnaire);
                Response<Boolean> booleanResponse= new Response<>();
                booleanResponse.setData(bool);
                booleanResponse.setResultMessage(new ResultMessage (Constants.ResultStatus.SUCCESS, new Message(0, "Questionnaire added")));

                Utils.serializeAndWrite2(booleanResponse, outputStream);
            } else if(subAction.equalsIgnoreCase(Constants.SUB_ACTIONS.deleteQuestionnaire.name())){
                logger.trace();
                QuestionnaireDao questionnaireDao = FactoryQuestionnaireOperations.getQuestionnaireOperationsProvider(user, Constants.DB_PROVIDER.POSTGRESQL);
                Long id = Long.valueOf(Integer.valueOf(request.getParameter("id")));
                Boolean bool = questionnaireDao.deleteQuestionnaire(id);
                Response<Boolean> booleanResponse = new Response<>();
                System.out.println(bool);
                booleanResponse.setData(bool);
                booleanResponse.setResultMessage(new ResultMessage(Constants.ResultStatus.SUCCESS, new Message(0, "Questionnaire deleted!")));

                Utils.serializeAndWrite2(booleanResponse, outputStream);
            } else if(subAction.equalsIgnoreCase(Constants.SUB_ACTIONS.startQuestionnaire.name())){
                QuestionnaireDao questionnaireDao = FactoryQuestionnaireOperations.getQuestionnaireOperationsProvider(user, Constants.DB_PROVIDER.POSTGRESQL);
                long questionnaire_id = Long.parseLong(request.getParameter("questionnaire_id"));
                logger.trace("questionnaire_id = " + questionnaire_id);
                Long id = questionnaireDao.startQuestionnaire(questionnaire_id);
                logger.trace("questionnaireDao.startQuestionnaire("+ questionnaire_id +") = " + id);
                Response<Long> booleanResponse= new Response<>();
                booleanResponse.setData(id);
                booleanResponse.setResultMessage(new ResultMessage (Constants.ResultStatus.SUCCESS, new Message(0, "Questionnaire started")));
                Utils.serializeAndWrite2(booleanResponse, outputStream);
            }
        } catch (UserDefinedException e){
            logger.trace();
            Response resp = new Response();
            resp.initFrom(e);
            Utils.serializeAndWrite2(resp, response.getOutputStream());
        } catch (Exception e){
            logger.trace();
            Response resp = new Response();
            resp.initFrom(new UserDefinedException(0,e.getMessage()));
            Utils.serializeAndWrite2(resp, response.getOutputStream());
        } finally {
            if (outputStream != null){
                outputStream.close();
            }
        }
    }
}
