package com.mv.controller.servlet;

import com.mv.domain.Response;
import com.mv.domain.User;
import com.mv.domain.exceptions.AuthenticationException;
import com.mv.domain.exceptions.UnrecognizedOperationException;
import com.mv.domain.exceptions.UserDefinedException;
import com.mv.utils.Constants;
import com.mv.utils.Utils;
import com.mv.utils.logger.FactoryLogger;
import com.mv.utils.logger.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ControllerServlet extends HttpServlet {

    Logger logger = FactoryLogger.getLogger();

    public void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request,response);
    }
    public void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request,response);
    }
    protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String action = request.getParameter("action");;
        RequestDispatcher requestDispatcher = null;
        response.setContentType("application/json");
        try {

            HttpSession session = request.getSession();
            User user = (User)session.getAttribute(Constants.LOGGED_USER);
            if (user == null) throw new AuthenticationException();

            if (action == null) throw  new UnrecognizedOperationException();
            logger.trace("action="+action);

            if(action.equalsIgnoreCase(Constants.SERVLET_PATHS.COMMON.name())) {
                requestDispatcher= request.getRequestDispatcher(Constants.SERVLET_PATHS.COMMON.value);
                requestDispatcher.forward(request, response);
            } else if (action.equalsIgnoreCase(Constants.SERVLET_PATHS.DICTIONARY.name())){
                logger.trace(Constants.SERVLET_PATHS.DICTIONARY.name());
                requestDispatcher = request.getRequestDispatcher(Constants.SERVLET_PATHS.DICTIONARY.value);
                requestDispatcher.forward(request, response);
            } else if (action.equalsIgnoreCase(Constants.SERVLET_PATHS.USERS.name())){
                logger.trace(Constants.SERVLET_PATHS.USERS.value);
                requestDispatcher = request.getRequestDispatcher(Constants.SERVLET_PATHS.USERS.value);
                requestDispatcher.forward(request, response);
            } else if (action.equalsIgnoreCase(Constants.SERVLET_PATHS.SURVEY.name())){
                logger.trace(Constants.SERVLET_PATHS.SURVEY.value);
                requestDispatcher = request.getRequestDispatcher(Constants.SERVLET_PATHS.SURVEY.value);
                requestDispatcher.forward(request, response);
            } else if (action.equalsIgnoreCase(Constants.SERVLET_PATHS.QUESTIONNAIRE.name())){
                logger.trace(Constants.SERVLET_PATHS.QUESTIONNAIRE.value);
                requestDispatcher = request.getRequestDispatcher(Constants.SERVLET_PATHS.QUESTIONNAIRE.value);
                requestDispatcher.forward(request, response);
            } else if (action.equalsIgnoreCase(Constants.SERVLET_PATHS.UTILS.name())){
                logger.trace(Constants.SERVLET_PATHS.UTILS.value);
                requestDispatcher = request.getRequestDispatcher(Constants.SERVLET_PATHS.UTILS.value);
                requestDispatcher.forward(request, response);
            }
        } catch (UserDefinedException ex){
            logger.error(ex);
            Response resp = new Response();
            resp.initFrom(ex);
            Utils.serializeAndWrite2(resp, response.getOutputStream());
        } catch (Exception e){
            logger.trace();
            Response resp = new Response();
            resp.initFrom(new UserDefinedException(0,e.getMessage()));
            Utils.serializeAndWrite2(resp, response.getOutputStream());
        }

    }
}