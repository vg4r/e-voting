package com.mv.controller.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mv.dao.UserDao;
import com.mv.domain.*;
import com.mv.domain.exceptions.AuthenticationException;
import com.mv.domain.exceptions.UnrecognizedOperationException;
import com.mv.domain.exceptions.UserDefinedException;
import com.mv.service.FactoryUsersOperations;
import com.mv.utils.Constants;
import com.mv.utils.Utils;
import com.mv.utils.logger.FactoryLogger;
import com.mv.utils.logger.Logger;
import com.mv.utils.serializers.DataContentSerializer;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.OutputStream;


@WebServlet(name = "UsersServlet")
public class UsersServlet extends HttpServlet {

    Logger logger = FactoryLogger.getLogger();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    public void processRequest(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
        response.setContentType("application/json");
        OutputStream outputStream = null;
        try {
            outputStream = response.getOutputStream();
            HttpSession session = request.getSession();
            User user = (User)session.getAttribute(Constants.LOGGED_USER);
            if (user == null) throw new AuthenticationException();

            String subAction = request.getParameter(Constants.SUB_ACTION);
            if (subAction.equalsIgnoreCase(Constants.SUB_ACTIONS.getUsersList.name())){
                UserDao userDao = FactoryUsersOperations.getUsersOperationsProvider(user, Constants.DB_PROVIDER.POSTGRESQL);
                int surveyId = Integer.parseInt(request.getParameter("surveyId") == null ? "0" : request.getParameter("surveyId"));
                int userTypeId = Integer.parseInt(request.getParameter("userTypeId") == null ? "0" : request.getParameter("userTypeId"));
                String search = request.getParameter("search") == null ? "" : request.getParameter("search") ;
                DataContent dictionaries = userDao.getUsers(surveyId, userTypeId, search);

                Utils.serializeAndWrite2(DataContent.class, new DataContentSerializer(), dictionaries, outputStream);

            } else if(subAction.equalsIgnoreCase(Constants.SUB_ACTIONS.addUser.name())){
                UserDao userDao = FactoryUsersOperations.getUsersOperationsProvider(user, Constants.DB_PROVIDER.POSTGRESQL);
                String json = request.getParameter("user");
                ObjectMapper objectMapper = new ObjectMapper();
                User users = objectMapper.readValue(json, User.class);
                response.setContentType("application/json");
                Boolean userAdd = userDao.addUser(users);
                Response<Boolean> integerResponse = new Response<>();
                integerResponse.setData(userAdd);
                integerResponse.setResultMessage(new ResultMessage(Constants.ResultStatus.SUCCESS, new Message(0, "User added")));
                Utils.serializeAndWrite2(integerResponse, outputStream);
            } else if(subAction.equalsIgnoreCase(Constants.SUB_ACTIONS.editUser.name())){
                UserDao userDao = FactoryUsersOperations.getUsersOperationsProvider(user, Constants.DB_PROVIDER.POSTGRESQL);
                String json = request.getParameter("user");
                ObjectMapper objectMapper = new ObjectMapper();
                User users = objectMapper.readValue(json, User.class);
                response.setContentType("application/json");
                Long id = userDao.editUser(users);
                Response<Long> integerResponse = new Response<>();
                integerResponse.setData(id);
                integerResponse.setResultMessage(new ResultMessage(Constants.ResultStatus.SUCCESS, new Message(0, "User edited")));
                Utils.serializeAndWrite2(integerResponse, outputStream);
            } else if(subAction.equalsIgnoreCase(Constants.SUB_ACTIONS.deleteUser.name())){
                UserDao userDao = FactoryUsersOperations.getUsersOperationsProvider(user, Constants.DB_PROVIDER.POSTGRESQL);
                String user_id = request.getParameter("user_id");
                response.setContentType("application/json");
                Long id = userDao.deleteUser(Long.parseLong(user_id));
                Response<Long> integerResponse = new Response<>();
                integerResponse.setData(id);
                integerResponse.setResultMessage(new ResultMessage(Constants.ResultStatus.SUCCESS, new Message(0, "User edited")));
                Utils.serializeAndWrite2(integerResponse, outputStream);
            } else if(subAction.equalsIgnoreCase(Constants.SUB_ACTIONS.resetPassword.name())){
                logger.trace();
                UserDao userDao = FactoryUsersOperations.getUsersOperationsProvider(user, Constants.DB_PROVIDER.POSTGRESQL);
                User resetPasswordOwner = new User();
                resetPasswordOwner.setUserId(Integer.parseInt(request.getParameter("user_id")));
                response.setContentType("application/json");
                Boolean resetPassword = userDao.resetPassword(resetPasswordOwner);
                Response<Boolean> integerResponse = new Response<>();
                integerResponse.setData(resetPassword);
                integerResponse.setResultMessage(new ResultMessage(Constants.ResultStatus.SUCCESS, new Message(0, "Password changed")));
                Utils.serializeAndWrite2(integerResponse, outputStream);
            } if (subAction.equalsIgnoreCase(Constants.SUB_ACTIONS.getParticipantDetails.name())){

                UserDao userDao = FactoryUsersOperations.getUsersOperationsProvider(user, Constants.DB_PROVIDER.POSTGRESQL);

                long userId = Long.parseLong(request.getParameter("userId"));
                DataContent dictionaries = userDao.getParticipantDetails(userId);
                Utils.serializeAndWrite2(DataContent.class, new DataContentSerializer(), dictionaries, outputStream);

            } else {
                    throw new UnrecognizedOperationException();
            }

        }catch (UserDefinedException e){
            logger.error(e);
            Response resp = new Response();
            resp.initFrom(e);
            Utils.serializeAndWrite2(resp, response.getOutputStream());
        }catch (Exception e){
            logger.error(e);
            Response resp = new Response();
            resp.initFrom(new UserDefinedException(0,e.getMessage()));
            Utils.serializeAndWrite2(resp, response.getOutputStream());
        } finally {
            if (outputStream != null){
                outputStream.close();
            }
        }

    }

}
