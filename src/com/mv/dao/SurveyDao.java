package com.mv.dao;

import com.mv.domain.DataContent;
import com.mv.domain.Survey;
import com.mv.domain.exceptions.UserDefinedException;

public interface SurveyDao {

    DataContent getSurvey() throws UserDefinedException;
    Long addSurvey(Survey survey) throws UserDefinedException;
    Boolean editSurvey(Survey survey) throws UserDefinedException;
    Boolean deleteSurvey(long id) throws UserDefinedException;
    Boolean stopSurvey(long id) throws UserDefinedException;
    DataContent [] getSurveyInfo(long id) throws UserDefinedException;

    DataContent[] getCurrentSurveyInfo() throws UserDefinedException;
}
