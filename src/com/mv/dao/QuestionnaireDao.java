package com.mv.dao;

import com.mv.domain.DataContent;
import com.mv.domain.DataContentArray;
import com.mv.domain.Questionnaire;
import com.mv.domain.QuestionnaireAnswer;
import com.mv.domain.exceptions.UserDefinedException;
import java.util.List;

public interface QuestionnaireDao {

    DataContent getQuestionnaire(Integer typeId, Integer privacyType, String startDate, Integer surveyId) throws UserDefinedException;

    DataContentArray getQuestionnaireDetail(Integer id) throws UserDefinedException;

    DataContent[] getQuestionnaireDetail2(long id) throws UserDefinedException;

    Boolean addQuestionnaire(Questionnaire questionnaire) throws UserDefinedException;

    Boolean editQuestionnaire(Questionnaire questionnaire) throws UserDefinedException;

    Boolean deleteQuestionnaire(long id) throws UserDefinedException;

    long startQuestionnaire(long questionnaireId) throws UserDefinedException;

    Boolean answerQuestionnaire(long questMoveId , QuestionnaireAnswer [] questionnaireAnswerList) throws UserDefinedException;

}
