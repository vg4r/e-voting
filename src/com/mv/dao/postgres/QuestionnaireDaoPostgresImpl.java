package com.mv.dao.postgres;

import com.mv.dao.BaseDao;
import com.mv.dao.QuestionnaireDao;
import com.mv.domain.*;
import com.mv.domain.exceptions.UserDefinedException;
import com.mv.utils.Converter;
import com.mv.utils.logger.FactoryLogger;
import com.mv.utils.logger.Logger;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class QuestionnaireDaoPostgresImpl extends BaseDao implements QuestionnaireDao {

    private static final Logger logger = FactoryLogger.getLogger();

    public QuestionnaireDaoPostgresImpl(User loggedUser){super(loggedUser);}
    @Override
    public DataContent getQuestionnaire(Integer typeId, Integer privacyType, String startDate, Integer surveyId) throws UserDefinedException {
        try(DBConnectorPostgresSQL connector = DBConnectorPostgresSQL.getInstance()){
            connector.openConnection();
            connector.callableStatement = connector.connection.prepareCall("{? = call evoting.get_questionnaire(?,?,?,?)}");
            connector.callableStatement.registerOutParameter(1, Types.OTHER);
            connector.callableStatement.setInt(2, typeId);
            connector.callableStatement.setInt(3, privacyType);
            connector.callableStatement.setString(4, startDate);
            connector.callableStatement.setInt(5, surveyId);
            connector.callableStatement.execute();

            connector.resultSet = (ResultSet) connector.callableStatement.getObject(1);

            return connector.readCursor();

        } catch (UserDefinedException e) {
            logger.error(e);
            throw e;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }
    }

    @Override
    public DataContentArray getQuestionnaireDetail(Integer id) throws UserDefinedException {
        try(DBConnectorPostgresSQL connector = DBConnectorPostgresSQL.getInstance()) {
            connector.openConnection();

            DataContentArray dataContentArray = new DataContentArray();

            PreparedStatement stmt = connector.connection.prepareStatement("select * from evoting.get_questionnaire_detail(?)");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            DataContent[] dataContents = new DataContent[2];
            if (rs.next())
            {

                Object o = rs.getObject(1);
                if (o instanceof ResultSet)
                {
                    connector.resultSet = (ResultSet)o;
                    dataContents[0]=connector.readCursor();
                }
            }
            if (rs.next())
            {

                Object o = rs.getObject(1);
                if (o instanceof ResultSet)
                {
                    logger.trace("2");
                    connector.resultSet = (ResultSet)o;
                    dataContents[1] = connector.readCursor();
                }
            }

            dataContentArray.setDataContents(dataContents);
            return dataContentArray;

        } catch (UserDefinedException e) {
            logger.error(e);
            throw e;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }
    }

    @Override
    public DataContent[] getQuestionnaireDetail2(long id) throws UserDefinedException {
        try(DBConnectorPostgresSQL c = DBConnectorPostgresSQL.getInstance()) {
            DataContent[] dataContents = new DataContent[2];
            c.openConnection();
            c.callableStatement = c.connection.prepareCall("{call evoting.get_questionnaire_detail2(?,?,?)}");
            /*c.callableStatement.registerOutParameter(1, Types.OTHER);*/
            c.callableStatement.setLong(1, id);
            c.callableStatement.registerOutParameter(2, Types.OTHER);
            c.callableStatement.registerOutParameter(3, Types.OTHER);
            c.callableStatement.execute();

            c.resultSet = (ResultSet)c.callableStatement.getObject(2);
            dataContents[0] = c.readCursor();
            c.closeResultSet();
            c.resultSet = (ResultSet)c.callableStatement.getObject(3);
            dataContents[1] = c.readCursor();

            return dataContents;
        } catch (UserDefinedException e) {
            logger.error(e);
            throw e;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }
    }

    @Override
    public Boolean addQuestionnaire(Questionnaire questionnaire) throws UserDefinedException{
        try(DBConnectorPostgresSQL connector = DBConnectorPostgresSQL.getInstance()){
            connector.openConnection();
            connector.callableStatement = connector.connection.prepareCall("{call evoting.add_questionnaire(?,?,?,?,?,?,?)}");
            Array variants = null;

            String [] arr = new String[questionnaire.getQuestionnaireVariants().length];
            for (int i = 0; i < questionnaire.getQuestionnaireVariants().length; i++) {
                QuestionnaireVariant v = questionnaire.getQuestionnaireVariants()[i];
                arr[i] = v.getContent();
            }

            if (arr.length > 0){
                variants = connector.connection.createArrayOf("varchar", arr);
            }
            connector.callableStatement.setInt(1, questionnaire.getTypeId());
            connector.callableStatement.setInt(2, questionnaire.getPrivacyType());
            connector.callableStatement.setString(3, questionnaire.getContent());
            connector.callableStatement.setInt(4, questionnaire.getTimeInterval());
            connector.callableStatement.setInt(5, questionnaire.getVoteCount());
            connector.callableStatement.setInt(6, questionnaire.getWinnerCount());
            connector.callableStatement.setObject(7, variants);
            connector.callableStatement.execute();
            connector.connection.commit();
            logger.trace();
            return true;

        } catch (SQLException e) {
            logger.error(e);
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }

    }

    @Override
    public Boolean editQuestionnaire(Questionnaire questionnaire) throws UserDefinedException {
        try(DBConnectorPostgresSQL connector = DBConnectorPostgresSQL.getInstance()){
            connector.openConnection();
            connector.callableStatement = connector.connection.prepareCall("call evoting.edit_questionniare(?,?,?,?,?,?)");

            connector.callableStatement.setInt(1, questionnaire.getId());
            connector.callableStatement.setInt(2, questionnaire.getTypeId());
            connector.callableStatement.setInt(3, questionnaire.getPrivacyType());
            connector.callableStatement.setString(4, questionnaire.getContent());
            connector.callableStatement.setInt(5, questionnaire.getTimeInterval());
            connector.callableStatement.setObject(6, questionnaire.getVariants());
            connector.callableStatement.executeUpdate();

            return true;

        } catch (SQLException e) {
            logger.error(e);
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }
    }

    @Override
    public Boolean deleteQuestionnaire(long id) throws UserDefinedException {
        try(DBConnectorPostgresSQL connector = DBConnectorPostgresSQL.getInstance()){
            connector.openConnection();
            connector.callableStatement = connector.connection.prepareCall("{call evoting.delete_questionnaire(?)}");
            connector.callableStatement.setLong(1, id);
            connector.callableStatement.execute();
            connector.connection.commit();
            logger.trace();
            return true;
        } catch (SQLException e) {
            logger.error(e);
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }
    }

    @Override
    public long startQuestionnaire(long questionnaireId) throws UserDefinedException {
        try(DBConnectorPostgresSQL connector = DBConnectorPostgresSQL.getInstance()){
            connector.openConnection();
            connector.callableStatement = connector.connection.prepareCall("{? = call evoting.start_questionnaire(?)}");
            connector.callableStatement.registerOutParameter(1, Types.NUMERIC);
            connector.callableStatement.setLong(2, questionnaireId);
            connector.callableStatement.execute();

            connector.connection.commit();

            return ((BigDecimal) connector.callableStatement.getObject(1)).longValue();
        } catch (SQLException e) {
            logger.error(e);
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }
    }

    @Override
    public Boolean answerQuestionnaire(long questMoveId , QuestionnaireAnswer [] questionnaireAnswerList) throws UserDefinedException {
        try(DBConnectorPostgresSQL connector = DBConnectorPostgresSQL.getInstance()){
            connector.openConnection();
            int counter = 0;
            for (QuestionnaireAnswer questionnaireAnswer : questionnaireAnswerList){
                counter++;
                connector.callableStatement = connector.connection.prepareCall("{call evoting.answer_quest(?,?,?,?,?)}");
                connector.callableStatement.setInt(1, counter);
                connector.callableStatement.setLong(2, getLoggedUser().getSurveyMember().getId());
                connector.callableStatement.setLong(3, questMoveId);
                connector.callableStatement.setLong(4, questionnaireAnswer.getVariantId());
                connector.callableStatement.setLong(5, questionnaireAnswer.getVoteCount());
                connector.callableStatement.execute();
                connector.closeCallableStatement();
                connector.connection.commit();
            }
            //connector.connection.commit();
            return true;
        } catch (SQLException e) {
            logger.error(e);
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }
    }

    /*---------------example for list----------------*/


/*    public int answerQuestionnaire2(long questMoveId , List<QuestionnaireAnswer> questionnaireAnswerList) throws UserDefinedException {
        try(DBConnectorPostgresSQL connector = DBConnectorPostgresSQL.getInstance()){
            connector.openConnection();
            QuestionnaireAnswer questionnaireAnswer = new QuestionnaireAnswer();
            Array a = connector.connection.createArrayOf("text", Converter.toPostgreVariants(questionnaireAnswer.getVariants()));
                connector.callableStatement = connector.connection.prepareCall("{call evoting.answer_quest(?,?,?,?)}");
                connector.callableStatement.setInt(1, getLoggedUser().getUserId());
                connector.callableStatement.setLong(2, questMoveId);
                connector.callableStatement.setArray(3,a);
                connector.callableStatement.execute();
                connector.closeCallableStatement();
            connector.connection.commit();
            return 1;
        } catch (SQLException e) {
            logger.error(e);
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }
    }
*/



}
