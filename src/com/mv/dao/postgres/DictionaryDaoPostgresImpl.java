package com.mv.dao.postgres;

import com.mv.dao.BaseDao;
import com.mv.dao.DictionaryDao;
import com.mv.domain.DataContent;
import com.mv.domain.Dictionary;
import com.mv.domain.User;
import com.mv.domain.exceptions.UserDefinedException;
import com.mv.utils.logger.FactoryLogger;
import com.mv.utils.logger.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;


public class DictionaryDaoPostgresImpl extends BaseDao implements DictionaryDao {

    private static final Logger logger = FactoryLogger.getLogger();

    public DictionaryDaoPostgresImpl(User loggedUser) {
        super(loggedUser);
    }

    @Override
    public DataContent getDictionaries(int dicTypeId) throws UserDefinedException {

        try ( DBConnectorPostgresSQL connector  = DBConnectorPostgresSQL.getInstance() ){
            connector.openConnection();
            connector.callableStatement = connector.connection.prepareCall("{? = call evoting.get_dictionaries(?)}");
            connector.callableStatement.registerOutParameter(1, Types.OTHER);
            connector.callableStatement.setInt(2, dicTypeId);
            connector.callableStatement.execute();
            connector.resultSet =  (ResultSet) connector.callableStatement.getObject(1);

            return connector.readCursor();
        } catch (SQLException e){
            logger.error(e);
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        } catch (UserDefinedException e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public DataContent getCountries() throws UserDefinedException {
        try ( DBConnectorPostgresSQL connector  = DBConnectorPostgresSQL.getInstance() ){
            connector.openConnection();
            connector.callableStatement = connector.connection.prepareCall("{? = call evoting.get_countries()}");
            connector.callableStatement.registerOutParameter(1, Types.OTHER);
            connector.callableStatement.execute();
            connector.resultSet =  (ResultSet) connector.callableStatement.getObject(1);

            return connector.readCursor();
        } catch (SQLException e){
            logger.error(e);
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        } catch (UserDefinedException e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public boolean addDictionary(Dictionary dictionary) throws UserDefinedException {

        try(DBConnectorPostgresSQL connector = DBConnectorPostgresSQL.getInstance()){
            connector.openConnection();

            connector.callableStatement = connector.connection.prepareCall("{call evoting.add_dictionary(?,?)}");
            connector.callableStatement.setInt(1, dictionary.getTypeId());
            connector.callableStatement.setString(2, dictionary.getName());

            connector.callableStatement.executeUpdate();

            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }
    }

    @Override
    public boolean editDictionary(Dictionary dictionary) throws UserDefinedException {

        try(DBConnectorPostgresSQL connector = DBConnectorPostgresSQL.getInstance()){
            connector.openConnection();
            connector.callableStatement = connector.connection.prepareCall("{call evoting.edit_dictionary(?,?,?)}");
            connector.callableStatement.setInt(1, dictionary.getId());
            connector.callableStatement.setInt(2, dictionary.getTypeId());
            connector.callableStatement.setString(3, dictionary.getName());
            connector.callableStatement.executeUpdate();

            return true;

        } catch (SQLException e) {
            logger.error(e);
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }
    }

    @Override
    public boolean deleteDictionary (long id) throws UserDefinedException {

        try(DBConnectorPostgresSQL connector = DBConnectorPostgresSQL.getInstance()){
            connector.openConnection();
            connector.callableStatement = connector.connection.prepareCall("{call evoting.delete_dictionary(?)}");
            connector.callableStatement.setLong(1, id);
            connector.callableStatement.executeUpdate();

            return true;

        } catch (SQLException e) {
            logger.error(e);
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }
    }
}
