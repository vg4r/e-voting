package com.mv.dao.postgres;

import com.mv.domain.DataContent;
import com.mv.domain.exceptions.UserDefinedException;
import com.mv.utils.Constants;
import com.mv.utils.logger.FactoryLogger;
import com.mv.utils.logger.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class DBConnectorPostgresSQL implements AutoCloseable{

    public Connection connection;
    public CallableStatement callableStatement;
    public ResultSet resultSet;

    static final Logger logger = FactoryLogger.getLogger();

    private DBConnectorPostgresSQL(){
        super();
    }

    public static DBConnectorPostgresSQL getInstance(){
        return new DBConnectorPostgresSQL();
    }

    @Override
    public void close() {
        logger.trace("AutoCloseable called close()");

        closeResultSet();

        closeCallableStatement();

        closeConnection();
    }

    public Connection openConnection(String dataSource) throws UserDefinedException, SQLException {
        try {
            Locale.setDefault(Locale.ENGLISH);
            Context ctx = new InitialContext();

            DataSource ds = (DataSource)ctx.lookup(dataSource);
            logger.trace("ds.getConnection(); ds = " );
            connection = ds.getConnection();
            connection.setAutoCommit(false);
            return connection;

        } catch (NamingException n) {
            logger.error(n);
            throw new UserDefinedException(1, n.getMessage());
        }
    }

    public Connection openConnection()throws UserDefinedException, SQLException{
        return openConnection(Constants.DATA_SOURCE_POSTGRESQL);
    }

    public void closeConnection(){
        try {
            if (connection != null) {
                if (!connection.isClosed()){
                    connection.close();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void closeCallableStatement(){
        try {
            if(callableStatement!= null){
                callableStatement.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void closeResultSet(){
        try {
            if(resultSet!= null){
                resultSet.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public DataContent readCursor() throws UserDefinedException, SQLException {
        if(resultSet == null) throw new UserDefinedException(1, "");
        DataContent content = new DataContent();
        ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
        Map<String, Integer> columns= new HashMap<>();
        for (int i = 0; i < resultSetMetaData.getColumnCount(); i++) {
            columns.put(resultSetMetaData.getColumnName(i + 1), i);
        }
        content.setColumns(columns);

        ArrayList<String[]> metaData = new ArrayList<>();
        while (resultSet.next()){
            String [] data = new String[columns.keySet().size()];
            for (int i = 0; i < columns.keySet().size(); i++) {
                data[i] = resultSet.getString(i+1);
            }
            metaData.add(data);
        }

        content.setMetaData(metaData);
        return content;
    }
}
