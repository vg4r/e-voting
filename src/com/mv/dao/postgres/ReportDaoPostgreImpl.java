package com.mv.dao.postgres;

import com.mv.dao.ReportDao;
import com.mv.domain.DataContent;
import com.mv.domain.exceptions.UserDefinedException;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.mv.dao.postgres.AuthServicePostgresImpl.logger;


public class ReportDaoPostgreImpl implements ReportDao {
    @Override
    public DataContent getQuestResult(int surveyId) throws UserDefinedException {

        try (DBConnectorPostgresSQL connector = DBConnectorPostgresSQL.getInstance()) {
            connector.openConnection();
            connector.callableStatement = connector.connection.prepareCall("{? = call evoting.get_report_by_survey(?)}");
            connector.callableStatement.registerOutParameter(1, Types.OTHER);
            connector.callableStatement.setInt(2, surveyId);
            connector.callableStatement.execute();
            ResultSet resultSet = (ResultSet) connector.callableStatement.getObject(1);
            if(resultSet == null) throw new UserDefinedException(1, "");
            DataContent content = new DataContent();
            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            Map<String, Integer> columns= new HashMap<>();
            for (int i = 0; i < resultSetMetaData.getColumnCount(); i++) {
                columns.put(resultSetMetaData.getColumnName(i + 1), i);
            }
            content.setColumns(columns);
            ArrayList<String[]> metaData = new ArrayList<>();
            int quest_move_id_old = 0;
            int vote_sum_old = 0;
            int counter = 0;
            while (resultSet.next()){
                if(quest_move_id_old == 0 && counter == 0) {//en birinci hal
                    quest_move_id_old = Integer.parseInt(resultSet.getString("quest_move_id"));
                    String [] data = new String[columns.keySet().size()]; //yazir 1ci setri
                    for (int i = 0; i < columns.keySet().size(); i++) {
                        data[i] = resultSet.getString(i+1);
                    }
                    metaData.add(data);
                    counter++;//=1
                    vote_sum_old = Integer.parseInt(resultSet.getString("votes_sum"));

                } else {
                    int quest_move_id = Integer.parseInt(resultSet.getString("quest_move_id"));
                    if ((quest_move_id == quest_move_id_old) &&
                            ((counter < Integer.parseInt(resultSet.getString("winner_count")))
                                    || (vote_sum_old == Integer.parseInt(resultSet.getString("votes_sum")))
                            )
                        ){
                        counter++;//2
                        String [] data = new String[columns.keySet().size()];
                       /* for (int i = 0; i < columns.keySet().size(); i++) {
                            data[i] = resultSet.getString(i+1);
                        }*/
//                        metaData.add(data);
                    }
                    else if(quest_move_id != quest_move_id_old){//yeni sual olanda
                        quest_move_id_old = Integer.parseInt(resultSet.getString("quest_move_id"));
                        String [] data = new String[columns.keySet().size()]; //yazir 1ci setri
                        vote_sum_old = Integer.parseInt(resultSet.getString("votes_sum"));
                        for (int i = 0; i < columns.keySet().size(); i++) {
                            data[i] = resultSet.getString(i+1);
                        }
                        metaData.add(data);
                        counter = 1;

                    }
                }
            }

            content.setMetaData(metaData);

            return content;



            //return connector.readCursor();

        } catch (UserDefinedException e) {
            logger.error(e);
            throw e;
        }catch (SQLException e){
            e.printStackTrace();
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }
    }

    @Override
    public DataContent[] getResultByQuest(int questId) throws UserDefinedException {
        try(DBConnectorPostgresSQL c = DBConnectorPostgresSQL.getInstance()) {
            DataContent[] dataContents = new DataContent[4];
            c.openConnection();
            c.callableStatement = c.connection.prepareCall("{call evoting.get_report_by_quest(?,?,?,?,?)}");
            c.callableStatement.setInt(1, questId);
            c.callableStatement.registerOutParameter(2, Types.OTHER);
            c.callableStatement.registerOutParameter(3, Types.OTHER);
            c.callableStatement.registerOutParameter(4, Types.OTHER);
            c.callableStatement.registerOutParameter(5, Types.OTHER);
            c.callableStatement.execute();

            c.resultSet = (ResultSet)c.callableStatement.getObject(2);
            dataContents[0] = c.readCursor();
            c.closeResultSet();
            c.resultSet = (ResultSet)c.callableStatement.getObject(3);
            dataContents[1] = c.readCursor();
            c.closeResultSet();
            /*---------------------------cursor 3--------------------------------------*/
            ResultSet resultSet = (ResultSet) c.callableStatement.getObject(4);
            if(resultSet == null) throw new UserDefinedException(1, "");
            DataContent content = new DataContent();
            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            Map<String, Integer> columns= new HashMap<>();
            for (int i = 0; i < resultSetMetaData.getColumnCount(); i++) {
                columns.put(resultSetMetaData.getColumnName(i + 1), i);
            }
            content.setColumns(columns);
            ArrayList<String[]> metaData = new ArrayList<>();
            int quest_move_id_old = 0;
            int vote_sum_old = 0;
            int counter = 0;
            while (resultSet.next()){
                // counter++;
                if(quest_move_id_old == 0 && counter == 0) {//en birinci hal
                    quest_move_id_old = Integer.parseInt(resultSet.getString("quest_move_id"));
                    String [] data = new String[columns.keySet().size()]; //yazir 1ci setri
                    for (int i = 0; i < columns.keySet().size(); i++) {
                        data[i] = resultSet.getString(i+1);
                    }
                    metaData.add(data);
                    counter++;//=1
                    vote_sum_old = Integer.parseInt(resultSet.getString("total_vote"));

                } else {
                    int quest_move_id = Integer.parseInt(resultSet.getString("quest_move_id"));
                    if ((quest_move_id == quest_move_id_old) &&
                            ((counter < Integer.parseInt(resultSet.getString("winner_count")))
                                    || (vote_sum_old == Integer.parseInt(resultSet.getString("total_vote")))
                            )
                            ){
                        counter++;//2
                        String [] data = new String[columns.keySet().size()];
                        for (int i = 0; i < columns.keySet().size(); i++) {
                            data[i] = resultSet.getString(i+1);
                        }
                        metaData.add(data);
                    }
                    else if(quest_move_id != quest_move_id_old){//yeni sual olanda
                        quest_move_id_old = Integer.parseInt(resultSet.getString("quest_move_id"));
                        String [] data = new String[columns.keySet().size()]; //yazir 1ci setri
                        vote_sum_old = Integer.parseInt(resultSet.getString("total_vote"));
                        for (int i = 0; i < columns.keySet().size(); i++) {
                            data[i] = resultSet.getString(i+1);
                        }
                        metaData.add(data);
                        counter = 1;
                    }
                }
            }
            content.setMetaData(metaData);
            dataContents[2] = content;
            c.closeResultSet();

            c.resultSet = (ResultSet)c.callableStatement.getObject(5);
            dataContents[3] = c.readCursor();

            return dataContents;

        } catch (UserDefinedException e) {
            logger.error(e);
            throw e;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new UserDefinedException(e.getErrorCode(), e.getMessage());
        }
    }
}
