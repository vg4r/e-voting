package com.mv.utils.logger;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SystemOutLogger implements Logger{

    @Override
    public void info(String text) {
        if (  LoggerUtils.CURRENT_LOG_LEVEL.level >= LoggerLevels.INFO.level )log(text);
    }

    @Override
    public void error(String text) {
        if (  LoggerUtils.CURRENT_LOG_LEVEL.level >= LoggerLevels.ERROR.level )log(  LoggerLevels.ERROR.name() + " | " + text);
    }

    @Override
    public void error(Exception e) {
        if (  LoggerUtils.CURRENT_LOG_LEVEL.level >= LoggerLevels.ERROR.level ){
            err(LoggerLevels.ERROR.name() + "| -------------------------------- StackTrace --------------------------------|");
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            err(sw.toString());
            err(LoggerLevels.ERROR.name() + "| -------------------------------- End of StackTrace --------------------------------|");
        }
    }

    @Override
    public void debug(String text) {
        if (  LoggerUtils.CURRENT_LOG_LEVEL.level >= LoggerLevels.DEBUG.level )log(text);
    }

    @Override
    public void trace(String text) {
        if (  LoggerUtils.CURRENT_LOG_LEVEL.level >= LoggerLevels.TRACE.level ){
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            log(LoggerLevels.TRACE.name() + " | " + stackTrace[2].getClassName() + "." + stackTrace[2].getMethodName() + " | Line number = " + stackTrace[2].getLineNumber() + " | text = " + text + " |");
        }
    }

    @Override
    public void trace() {
        if (  LoggerUtils.CURRENT_LOG_LEVEL.level >= LoggerLevels.TRACE.level ){
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            log(LoggerLevels.TRACE.name() + " | " + stackTrace[2].getClassName() + "." + stackTrace[2].getMethodName() + " | Line number = " + stackTrace[2].getLineNumber());
        }
    }

    @Override
    public void log(String text) {
        SimpleDateFormat sdf = new SimpleDateFormat(LoggerUtils.CURRENT_DATE_TIME_FORMAT);
        System.out.println( "[" + sdf.format(new Date()) + "] " +  text);
    }

    public void err(String text) {
        SimpleDateFormat sdf = new SimpleDateFormat(LoggerUtils.CURRENT_DATE_TIME_FORMAT);
        System.err.println( "[" + sdf.format(new Date()) + "] " +  text);
    }
}