package com.mv.utils.logger;

public class FactoryLogger{

    public static Logger getLogger(){
        if (LoggerUtils.CURRENT_LOGGER_TYPE.equals(LoggerTypes.SYSTEM_OUT)){
            return new SystemOutLogger();
        }

        throw new UnsupportedOperationException();
    }

}