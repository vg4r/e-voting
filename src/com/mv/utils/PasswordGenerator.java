package com.mv.utils;

import java.nio.ByteBuffer;
import java.util.UUID;


public class PasswordGenerator {

    public static final String getUserPassword() {
        UUID uuid = UUID.randomUUID();
        long l = ByteBuffer.wrap(uuid.toString().getBytes()).getLong();
        return Long.toString(l, Character.MAX_RADIX).substring(0,7);
    }
}
