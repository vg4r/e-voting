package com.mv.utils;

import com.mv.domain.SurveyMember;
import com.mv.domain.Variant;

import java.util.List;


public class Converter {

    public static final Object[][] toPostgresSurveyMembers(List<SurveyMember> surveyMembers){
        Object[][] objects = new Object[surveyMembers.size()][5];

        for(int i=0; i<surveyMembers.size(); i++){
            objects[i][0] = surveyMembers.get(i).getCountry().getId();
            objects[i][1] = surveyMembers.get(i).getUsername();
            objects[i][2] = surveyMembers.get(i).getPassword();
            objects[i][3] = surveyMembers.get(i).getPasswordOriginal();
            objects[i][4] = surveyMembers.get(i).getUserId();
        }
        return objects;
    }
}
